<div class='wrap'>
	<div id="icon-edit-pages" class="icon32 icon32-posts-page"><br></div>
	<h2>WP Experiments Settings</h2>
	
	<?php if(strlen($license) == 0): ?>
	<tr valign="top">
		<?php $started = get_option("wpexpro__installed"); 
			$current = current_time("timestamp");
			global $_FREE_TRIAL_LENGTH;
			$remaining = ($_FREE_TRIAL_LENGTH*24*60*60) - ($current - $started);
			if($remaining < 0):
		?>
			<p class="alert">Free Trial has ended. <a href='https://wpexperiments.com'>Upgrade Now!</a></p>
		<?php else: ?>
			<p class="alert">Free Trial ends in <?php echo ceil($remaining/(24*60*60)) . " days"; ?></p>
		<?php endif; ?>
	<?php endif; ?>
	<form method="post">
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row"><label for="blogname">License Key</label></th>
					<td>
						<input name="license" type="text" value="<?php echo wpexpro_key_hide($license); ?>" class="regular-text">
						<p class="description">Enter your license key to activate the plugin.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Domain Limit</label></th>
					<td>
						<?php echo $num_sites == -1 ? "Unlimited" : $num_sites ; ?><br/>
						<p class="description">How many domains you can use this license on.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Simultaneous Experiment Limit</label></th>
					<td>
						<?php echo $num_exps == -1 ? "Unlimited" : $num_sites ; ?><br/>
						<p class="description">How many simultaneous experiments you can run at a time.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Test Case Limit</label></th>
					<td>
						<?php echo $num_tests == -1 ? "Unlimited" : $num_tests ; ?><br/>
						<p class="description">How many test cases you can have per experiment.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Minimum User Role</label></th>
					<td>
						<select name="role">
							<option value="administrator" <?php if($role == "administrator"):?>selected<?php endif; ?>>Administrator</option>
							<option value="editor" <?php if($role == "editor"):?>selected<?php endif; ?>>Editor</option>
							<option value="author" <?php if($role == "author"):?>selected<?php endif; ?>>Author</option>
							<option value="contributor" <?php if($role == "contributor"):?>selected<?php endif; ?>>Contributor</option>
							<option value="subscriber" <?php if($role == "subscriber"):?>selected<?php endif; ?>>Subscriber</option>
						</select>
						<p class="description">Any users less than this role won't see the experiments tab at all.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Ignore logged in visitors</label></th>
					<td>
						<input name="exclude_users" type="checkbox" value="1" <?php if($exclude_users): ?>checked<?php endif; ?>> Ignore logged in visitors
						<p class="description">Ignore impressions and conversions from visitors who are logged into Wordpress.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Ignore search engine bots</label></th>
					<td>
						<input name="exclude_bots" type="checkbox" value="1" <?php if($exclude_bots): ?>checked<?php endif; ?>> Ignore search engine bots
						<p class="description">Ignore impressions and conversions from search engine bots.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Test Bucket</label></th>
					<td>
						<input name="bucket" type="checkbox" value="1" <?php if($bucket): ?>checked<?php endif; ?>> Send return visitors to the same test case.
						<p class="description">Enabling this option prevents a visitor from seeing multiple version of a page.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Help or Support?</label></th>
					<td>
						Do you need help or support? Check out <a target="_blank" href="http://www.wpexperiments.com">http://www.wpexperiments.com</a> or email me at <a href="mailto:jason@wpexperiments.com">jason@wpexperiments.com</a>.
					</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" class="button-primary" name="save" value="Save Settings" /></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>