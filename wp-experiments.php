<?php
/*
	Plugin Name: WP Experiments Pro
	Plugin URI: http://wpexperiments.com
	Description: Use WP Experiments to test your pages and posts to maximize your conversions.
	Author: Jason
	Author URI: http://wpexperiments.com
	Version: 1.26
	License: GPLv3
*/

global $api_url_base;
$api_url_base = "https://wpexperiments.com/api/";

global $wpexpro_db_version;
$wpexpro_db_version = "0.0.2";

$_FREE_TRIAL_LENGTH = 14;

/* Update Settings */
$api_url = $api_url_base.'/admin/update/';
$plugin_slug = plugin_basename(__FILE__);

// Take over the update check
add_filter('pre_set_site_transient_update_plugins', 'wpexpro_check_for_plugin_update');
function wpexpro_check_for_plugin_update($checked_data) {
	global $api_url, $plugin_slug, $wp_version;
	//Comment out these two lines during testing.
	if (empty($checked_data->checked))
		return $checked_data;
	
	$args = array(
		'slug' => $plugin_slug,
		'version' => $checked_data->checked[$plugin_slug]
	);
	$request_string = array(
			'body' => array(
				'action' => 'basic_check', 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')			
		);
	
	// Start checking for an update
	$raw_response = wp_remote_post($api_url, $request_string);
	
	if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
		$response = unserialize($raw_response['body']);
	
	if (is_object($response) && !empty($response)) // Feed the update data into WP updater
		$checked_data->response[$plugin_slug] = $response;

	return $checked_data;
}

// Take over the Plugin info screen
add_filter('plugins_api', 'wpexpro_plugin_api_call', 10, 3);
function wpexpro_plugin_api_call($def, $action, $args) {
	global $plugin_slug, $api_url, $wp_version;
	if (!isset($args->slug) || ($args->slug != $plugin_slug))
		return false;
	
	// Get the current version
	$plugin_info = get_site_transient('update_plugins');
	$current_version = $plugin_info->checked[$plugin_slug .'/'. $plugin_slug .'.php'];
	$args->version = $current_version;
	
	$request_string = array(
			'body' => array(
				'action' => $action, 
				'request' => serialize($args),
				'api-key' => md5(get_bloginfo('url'))
			),
			'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
		);
	
	$request = wp_remote_post($api_url, $request_string);
	
	if (is_wp_error($request)) {
		$res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
	} else {
		$res = unserialize($request['body']);
		
		if ($res === false)
			$res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
	}
	
	return $res;
}

// Don't tell wordpress.org about me
add_filter('http_request_args', 'filter_parse_arr', 10, 2);
function filter_parse_arr($arr, $url) {
	global $plugin_slug;
	// hide from wordpress.org
	if ($url == "http://api.wordpress.org/plugins/update-check/1.0/") {
		$plugs = unserialize($arr['body']['plugins']);
		unset($plugs->plugins[$plugin_slug]);
		$arr['body']['plugins'] = serialize($plugs);
	}
	return $arr;
}

/* Installation Functions */
register_activation_hook( __FILE__, 'wpexpro_install' );

function wpexpro_install() {
	global $wpdb;
	global $wpexpro_db_version;
	global $api_url_base;
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$cur_db_version = get_option("wpexpro__db_version");
	if($cur_db_version != $wpexpro_db_version) {
		$table_name = $wpdb->prefix . "wpexpro_exps";
		$sql = "CREATE TABLE $table_name(
			id int(11) PRIMARY KEY auto_increment,
			post_id varchar(8),
			user_id int(11),
			description varchar(512),
			title varchar(100),
			started_ts int(11),
			status int(11),
			old_status int(11),
			goal_type int(11),
			goal varchar(255),
			INDEX(post_id)
		);";
		dbDelta( $sql );

		$table_name = $wpdb->prefix. "wpexpro_tests";
		$sql = "CREATE TABLE $table_name(
			id int(11) PRIMARY KEY auto_increment,
			exp_id int(11) NOT NULL,
			name varchar(255),
			description text,
			impressions int(11) DEFAULT 0,
			conversions int(11) DEFAULT 0,
			data text,
			INDEX(exp_id)
		);";
		dbDelta( $sql );

		$table_name = $wpdb->prefix."wpexpro_stats";
		$sql = "CREATE TABLE $table_name(
			id int(11) PRIMARY KEY auto_increment,
			session_id varchar(100),
			test_id int(11) NOT NULL,
			ts int(11) NOT NULL,
			ip varchar(17),
			converted tinyint(4) DEFAULT 0,
			leave_ts int(11),
			INDEX(test_id),
			INDEX(ts)
		);";

		dbDelta( $sql );
	}
	
	update_option( "wpexpro__installed", current_time('timestamp') );
	update_option( "wpexpro__db_version", $wpexpro_db_version );
	update_option( "wpexpro__api_url", $api_url_base);
}

include('user-agents.php');
include('wpexpro.class.php');

function wpexpro_key_hide($k) {
	$k = trim(substr($k, -4));
	if(!$k) {
		return "";
	} else {
		return "XXXX-XXXX-XXXX-".substr($k, -4);
	}
}
function wpexpro_ehe($s) {
	esc_html_e($s,'wpexpro');
}