// keep track of our test steps so we can
// access them eaisly
_wpexproTestSteps = {};
wpexproDoHover = true;
wpexproDoOriginalHover = false;
wpexproOutlineOffset = 10;
wpexproCurrentHTMLEditor = null;
wpexproDeletedTests = [];
wpexproCurrentChartData = null; // the current data we are using in our chart
// keep track of the ids we've generated just to guarentee uniqueness
wpexproGenIds = [];

// All the functions that actually do stuff are in other places

// wpexpro_admin.js -- contains the functions neccessary for the admin dashboard

// wpexpro_helpers.js -- functions that are used all over the place

// wpexpro_test_cases.js -- the meat of the dashboard, used to setup and edit test cases

// wpexpro_original.js -- used to setup and interact with the original

// wpexpro_enable_exp.js -- is added to every page which checks if it's being displayed as 
//                                          an iframe and tells the dashboard that it's there and ready