// borrowed from http://stackoverflow.com/a/10734934/1128407
wpexproGenId = function() {
    // always start with a letter (for DOM friendlyness)
    var idstr=String.fromCharCode(Math.floor((Math.random()*25)+65));
    do {
        // between numbers and characters (48 is 0 and 90 is Z (42-48 = 90)
        var ascicode=Math.floor((Math.random()*42)+48);
        if (ascicode<58 || ascicode>64){
            // exclude all chars between : (58) and @ (64)
            idstr+=String.fromCharCode(ascicode);
        }
    } while (idstr.length<32);

    return (wpexproGenIds.indexOf(idstr) == -1 ? idstr : wpexproGenId());
};

// This works fine but tends to give WAY more styles then we really want...
//based on http://stackoverflow.com/a/5830517/1128407
wpexproGetCss = function(a,iframeDoc){
    var sheets = iframeDoc.styleSheets, o = {};
    for(var i in sheets) {
        var rules = sheets[i].rules || sheets[i].cssRules;
        iframeWindow = wpexproIframeWindow(iframeDoc);
        for(var r in rules) {
            try {
                if(iframeWindow.wpexpro_is(a,rules[r].selectorText)) {
                    o = jQuery.extend(o, wpexproCSSToObj(rules[r].style), wpexproCSSToObj(a.attr('style')));
                }else {
                    // console.log(rules[r].selectorText,"no");
                }
            } catch(err) {
                // Do something with the peusdo classes at some point. 
                // $.is() will fail with the :hover,etc css selectors. We'll need to
                // check if the hover effects this element somehow. Probably
                // for simplicity, only worry about this element when it's hovered
                // console.log("Invalid selector: " + rules[r].selectorText);
            }
        }
    }
    return o;
};

// Change a CSS string to a javascript object
wpexproCSSToObj = function(css){
    var s = {};
    if(!css) return s;
    if(css instanceof CSSStyleDeclaration || css.constructor.toString().match(/CSSStyleDeclaration/)) {
        for(var i=0; i<css.length; i++) {
            s[css.item(i).toLowerCase()] = (css[css.item(i)]);
        }
    } else if(typeof css == "string") {
        css = css.split(";");
        for (var ix in css) {
            var l = css[ix].split(":");
            var k = jQuery.trim(l[0]).toLowerCase();
            var v = jQuery.trim(l[1]);
            if(k.length > 0 && v.length > 0)
                s[k] = v;
        }
    }
    return s;
};

// This is needed because we have a reverse
// parent selector which isn't supported in jQuery
wpexproGetElement = function(selector,iframeDoc) {
    if(typeof selector == "undefined") return [];
    if(selector.match(/</)) {
        var pieces = selector.split("<");
        $elm = jQuery(pieces[0],iframeDoc);
        for(var i = 0; i < pieces.length - 1; i++) {
            $elm = $elm.parent();
            if($elm.length === 0)
                break;
        }
        return $elm;
    } else {
        return jQuery(selector,iframeDoc);
    }
};

// Get a good selector for this element so that 
// we can find it again
wpexproGetSelector = function($elm) {
    var id = $elm.attr("id");
    if(id) return "#"+id;

    //look for a child with an id
    $cid = $elm.find("[id]");
    if($cid.length > 0) {
        //found it, now find how far away we are
        var sel = "#"+$cid.attr("id");
        for(var p = $cid.parent().get(0); p.tagName.toLowerCase() != "body"; p = jQuery(p).parent().get(0)) {
            $p = jQuery(p);
            sel += " < " + p.tagName.toLowerCase();
            if($p[0] == $elm[0]) {
                break;
            }
        }
        return sel;
    }

    //This is going to be less accurate
    var s = "";
    var elm = $elm.get(0);
    while(1){
        if(elm.id) {
            s = "#"+elm.id+s;
            break;
        }else if(elm.tagName == "BODY") {
            s = "body" + s;
            break;
        } else {
            var child_no = wpexproChildNum(elm);
            s = ">" + elm.tagName.toLowerCase() + ":nth-of-type("+child_no+")" + s;
        }
        elm = elm.parentNode;
    }
    return s;
};

//This function returns this elements child number of it's
// parent. This is used to find our reverse parent selector
wpexproChildNum = function(elm) {
    count = 1;
    for($e = jQuery(elm).prev(); $e.length > 0; $e = $e.prev()) {
        if($e.get(0).tagName == elm.tagName) {
            count++;
        }
    }
    return count;
};

// Returns the window object of a given iframe object
wpexproIframeWindow= function(iframe_object) {
    if (iframe_object.defaultView) {
        return iframe_object.defaultView;
    }
    if (iframe_object.parentWindow) {
        return iframe_object.parentWindow;
    }

    return undefined;
};