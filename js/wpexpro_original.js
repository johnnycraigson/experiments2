// Setup the elements inside of the original iFrame doc to respond
// to mouse events (hovering and clicking)
wpexproSetupOriginalElements = function($elms,iframeDoc) {
	$elms.css("cursor","pointer !important");
	$elms.hover(function(){
		if(!wpexproDoOriginalHover || jQuery(".reveal-modal-bg:visible").length > 0) return;
		if(jQuery(this).is("[id^=wpexpro-overlay]")) return;
		if(!jQuery(this).is(":visible")) return;
		jQuery(this).addClass("_wpexpro-mouseover");
		wpexproHighlightOriginalDeepest(iframeDoc);
	},function() {
		if(!wpexproDoOriginalHover || jQuery(".reveal-modal-bg:visible").length > 0) return;
		if(jQuery(this).is("[id^=wpexpro-overlay]")) return;
		if(!jQuery(this).is(":visible")) return;
		jQuery(".wpexpro-mouseover",iframeDoc).removeClass("_wpexpro-mouseover");
		wpexproHighlightOriginalDeepest(iframeDoc);
	}).click(function(ev){ wpexproOriginalHoverClick(ev,iframeDoc); return false; });
};

// This is called when the hover overlay is clicked
wpexproOriginalHoverClick = function(ev,iframeDoc) {
	ev.preventDefault();
	ev.stopPropagation();
	wpexproDoOriginalHover = false;
	jQuery("._wpexpro-mouseover",iframeDoc).removeClass("_wpexpro-mouseover");
	jQuery(".wpexpro-mouseover",iframeDoc).removeClass("wpexpro-mouseover");
	jQuery("#wpexpro-overlay-l, #wpexpro-overlay-r, #wpexpro-overlay-t, #wpexpro-overlay-b",iframeDoc).hide();
	wpexproOriginalHighlightGoal(iframeDoc);
	return false;
};

// This looks at all the elements that are currently being
// hovered over and finds the "deepest" one and adds
// the class to indicate it.
wpexproHighlightOriginalDeepest= function(iframeDoc) {
	jQuery(".wpexpro-mouseover",iframeDoc).removeClass("wpexpro-mouseover");
	var deepest = jQuery("._wpexpro-mouseover:last",iframeDoc);
	deepest.addClass("wpexpro-mouseover");
	jQuery(".wpexpro-exp-goal-focused").val(wpexproGetSelector(deepest,iframeDoc));
};

wpexproOriginalHighlightGoal = function(iframeDoc) {
	var sel = jQuery(".wpexpro-tab-pane:visible .wpexpro-exp-goal").val();
	var $e = wpexproGetElement(sel,iframeDoc);
	if($e.length > 0) {
		if(!$e.is(":visible")) {
			jQuery("#wpexpro-goal-overlay-l, #wpexpro-goal-overlay-r, #wpexpro-goal-overlay-t, #wpexpro-goal-overlay-b",iframeDoc).hide();
		} else {
			//body is the offset parent of the overlay
			var eo = $e.offset();
			var ew = $e.width();
			var eh = $e.height();

			var ept = parseInt($e.css("padding-top"),10);
			var epb = parseInt($e.css("padding-bottom"),10);
			var epl = parseInt($e.css("padding-left"),10);
			var epr = parseInt($e.css("padding-right"),10);

			eh += ept + epb;
			ew += epr + epl;

			if(eh === 0 || ew === 0) return;
			
			//Get all the elements offsets based on margin and border
			var totalOffset = wpexproGetOffset($e);

			jQuery("#wpexpro-goal-overlay-l",iframeDoc).css({
				top: eo.top-wpexproOutlineOffset-totalOffset.top,
				left: eo.left-wpexproOutlineOffset,
				height: eh+2+2*wpexproOutlineOffset,
				width: '2px'
			}).show();
			jQuery("#wpexpro-goal-overlay-r",iframeDoc).css({
				top: eo.top-wpexproOutlineOffset-totalOffset.top,
				left: eo.left - (wpexproOutlineOffset) + ew + 2*wpexproOutlineOffset,
				height: eh+2+2*wpexproOutlineOffset,
				width: '2px'
			}).show();
			jQuery("#wpexpro-goal-overlay-t",iframeDoc).css({
				top: eo.top-wpexproOutlineOffset-totalOffset.top,
				left: eo.left-wpexproOutlineOffset,
				height: '2px',
				width: ew + 2*wpexproOutlineOffset
			}).show();
			jQuery("#wpexpro-goal-overlay-b",iframeDoc).css({
				top: eo.top - wpexproOutlineOffset-totalOffset.top + eh + 2*wpexproOutlineOffset,
				left: eo.left - wpexproOutlineOffset,
				height: '2px',
				width: ew + 2*wpexproOutlineOffset
			}).show();
		}
	}
};