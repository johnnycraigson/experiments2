// Enable the element highlighting in the admin experiment viewer
// This checks if the function exists and calls it when we are ready. 
// Only happens when we are in an iframe
var wpexpro_is_admin = false;
var wpexpro_enable_exp = function() {
	if(window.parent && window.parent.wpexproEnableIframe) {
		wpexpro_is_admin = true;
		window.parent.wpexproEnableIframe(this);
	} else {
		//set up tracking
		if(typeof _wpexproData !== "undefined" && _wpexproData.tests) {
			// tests contains a hash with key of the element selector and 
			// value as an array of test ids whose goal is that element
			for(var sel in _wpexproData.tests) {
				var $elm;
				//wrap in try/catch to ensure that a bad selector doesn't kill us
				try {
					$elm = wpexproGetElement(sel);
					if($elm.length === 0) {
						continue;
					}
				}catch(err) {
					continue;
				}

				//set the tests associated with this element so we can get them later
				$elm.data("wpexpro-tests", _wpexproData.tests[sel].join(","));

				//get the stat ids associated with the tests associated with this element
				var stat_ids = [];
				for(var k in _wpexproData.tests[sel]) {
					var test_id = _wpexproData.tests[sel][k];
					stat_ids.push(_wpexproData.stats[test_id]);
				}

				// store them so we can get them later
				$elm.data("wpexpro-stats", stat_ids.join(","));
				$elm.click(wpexproGoalClicked);
			}
		}

		// start a stat poll to update the "leave" timestamp for these tests. It will
		// stop when they leave the page and therefore we know how long they
		// stayed
		if(typeof _wpexproData !== "undefined" && _wpexproData.stats) {
			var _stat_ids = [];
			for(var _k in _wpexproData.stats) {
				_stat_ids.push(_wpexproData.stats[_k]);
			}
			if(_stat_ids.length > 0) {
				window.setTimeout("wpexproFinishPoll("+JSON.stringify(_stat_ids)+");",10000);
			}
		}
	}
};

var wpexproEval = function(s) {if(s) {eval(s); } };

var wpexpro_is = function($e,sel) {
	return $e.is(sel);
};
jQuery(document).ready(wpexpro_enable_exp);

wpexproFinishPoll = function(stat_ids) {
	data = {
		"action": "trigger_check",
		"wpexpro-finish": 1,
		"wpexpro-stats": stat_ids
	};
	jQuery.ajax({
		url: ajaxurl,
		data: data,
		success: function() {
			window.setTimeout("wpexproFinishPoll("+JSON.stringify(stat_ids)+");",10000);
		}
	});
};

wpexproGoalClicked = function() {
	var $this = jQuery(this);
	if($this.data("wpexpro_done")) {
		return true;
	} else {
		data = {
			"action": "trigger_check",
			"wpexpro-record": 1,
			"wpexpro-tests": $this.data("wpexpro-tests").split(","),
			"wpexpro-stats": $this.data("wpexpro-stats").split(",")
		};
		jQuery.ajax({
			url: ajaxurl,
			data: data,
			success: function() {
				$this.data("wpexpro_done", 1);
				if($this.get(0).tagName == "A") {
					wpexproClickLink($this.get(0));
				} else {
					$this.trigger('click');
				}
			}
		});
		return false;
	}
};

//This is required because javascript won't do the normal
// action on the links because of XSS... so we fake it
// From http://stackoverflow.com/a/902838/1128407
function wpexproClickLink(link) {
    var cancelled = false;

    if (document.createEvent) {
        var event = document.createEvent("MouseEvents");
        event.initMouseEvent("click", true, true, window,
            0, 0, 0, 0, 0,
            false, false, false, false,
            0, null);
        cancelled = !link.dispatchEvent(event);
    }
    else if (link.fireEvent) {
        cancelled = !link.fireEvent("onclick");
    }

    if (!cancelled) {
        window.location = link.href;
    }
}