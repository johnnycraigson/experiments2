(function($) {
	$(document).ready(function() {
		if($(".wpexpro-nav-tab-wrapper").length === 0) return;

		$(".wpexpro-nav-tab-wrapper").on("click",".wpexpro-tab:not(#wpexpro-add-test-case)",function() {
			
			$(".wpexpro-tab-pane").hide();
			var $pane = $($(this).attr("href"));
			$pane.show();
			wpexproResizeVisibleIframe();
			$(".nav-tab-active").removeClass("nav-tab-active");
			$(this).addClass("nav-tab-active");

			if(jQuery("#wpexpro-chart").is(":visible")) {
				//we should unpause it if it was paused because of a tab-click
				if(jQuery(".wpexpro-chart-refresh").hasClass("tab-paused"))  {
					jQuery(".wpexpro-chart-refresh").click();
					jQuery(".wpexpro-chart-refresh").removeClass("tab-paused");
				}
			} else {
				//we should pause it if it's not
				if(!jQuery(".wpexpro-chart-refresh").hasClass("paused"))  {
					jQuery(".wpexpro-chart-refresh").click();
					jQuery(".wpexpro-chart-refresh").addClass("tab-paused");
				}
			}

			//Check if there are any iframes to load
			$pane.children("iframe.wpexpro-exp[_src]").each(function(i,e){
				var src = $(e).attr("_src");
				var srcuri = new URI(src);
				srcuri.addSearch("wpexpro-exp-test-id",$pane.data("wpexpro-num"));
				$(e).attr("src",srcuri.toString());
				$(e).removeAttr("_src");
			});

			return false;
		});

		$("#wpexpro-submit").click(wpexproDoSubmit);

		$("#wpexpro-add-test-case").click(wpexproAddTestCase);

		$(window).resize(wpexproResizeVisibleIframe);
		wpexproInit();
	});
})(jQuery);

// Add all the test cases to the admin page
wpexproInit = function() {
	for(i = 0; i < wpexproData.tests.length; i++) {
		wpexproAddTestCase(wpexproData.tests[i], true, i);
	}
	wpexproInstallChartTemplate();
	wpexproDrawChart();

	jQuery(window).resize(function () {
		wpexproWaitForFinalEvent(function(){
			wpexproDrawChart(undefined, true);
		}, 500, "wpexpChartResizeRedraw");
	});

	// Setup our date picker
	jQuery(".wpexpro-date-picker").click(function(ev) {
		jQuery(this).children(".wpexpro-date-picker-cont").slideToggle();
		ev.stopPropagation();
	});
	jQuery(".wpexpro-date-picker-cont").click(function(ev){
		ev.preventDefault();
		ev.stopPropagation();
	});
	jQuery(".wpexpro-date-picker-adjust").click(function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		var cont = jQuery(this).closest("div");
		var from = cont.find("input.wpexpro-date-picker-from").removeClass("error").val();
		var to = cont.find("input.wpexpro-date-picker-to").removeClass("error").val();
		var header = cont.closest("h2");
		header.find("span.wpexpro-date-picker-label").text("From " + from + " to " + to);
		cont.slideUp();
		wpexproUpdateChart(from,to);
	});

	jQuery(".wpexpro-date-picker-all").click(function(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		var cont = jQuery(this).closest("div");
		cont.find("input.wpexpro-date-picker-from").val("").removeClass("error");
		cont.find("input.wpexpro-date-picker-to").val("").removeClass("error");
		var header = cont.closest("h2");
		header.find("span.wpexpro-date-picker-label").text("All Time");
		cont.slideUp();
		wpexproUpdateChart(0,0);
	});

	jQuery("input.wpexpro-date-picker-to,input.wpexpro-date-picker-from").keyup(function(ev){
		if ( ev.which == 27 ) {
			jQuery(".wpexpro-date-picker-cont:visible").slideUp();
		}
		if ( ev.which == 13 ) {
			ev.preventDefault();
			if(jQuery("input.wpexpro-date-picker-from").val() !== "" && jQuery("input.wpexpro-date-picker-to").val() !== "") {
				jQuery(".wpexpro-date-picker-adjust").click();
			}
		}
	});

	jQuery("body").click(function(){
		if(jQuery(".wpexpro-date-picker-cont:visible").length > 0) {
			jQuery(".wpexpro-date-picker-cont:visible").slideUp();
		}
	});
	jQuery(".wpexpro-chart-refresh").click(function(){
		wpexproPaused = !wpexproPaused;
		jQuery(".wpexpro-chart-refresh").toggleClass("paused",wpexproPaused);
		if(wpexproChartTimeTo == -1) wpexproChartRefresh();
	});

	//sum our initail total conversions to avoid unneccesary chart refreshes
	jQuery(".wpexpro-chart-data-conversions").each(function(){
		wpexproChartTotalConvs += jQuery(this).data("total");
	});
	if(wpexproChartTimeTo == -1) wpexproChartRefresh();

};

wpexproChartTimeTo = -1;
wpexproChartTimeLabels = "";
wpexproChartTotalConvs = 0;
wpexproChartStart = 0;
wpexproChartEnd = 0;
wpexproPaused = false;
wpexproChartRefresh = function() {
	if(wpexproPaused) {
		wpexproChartTimeTo = -1;
		return;
	}
	var elm = jQuery(".wpexpro-chart-refresh > span");
	var ref = "Refreshing...";
	if(elm.length === 0) {
		jQuery(".wpexpro-chart-refresh").html("Refreshing in <span>15</span>s");
	}  else {
		var txt = elm.text();
		remaining = parseInt(txt,10) - 1;
		if(remaining == -1) {
			elm.parent().text(ref);
			wpexproUpdateChart(wpexproChartStart,wpexproChartEnd,true);
			wpexproChartTimeTo = -1;
			return;
		} else {
			elm.text(remaining);
		}
	}

	wpexproChartTimeTo = window.setTimeout("wpexproChartRefresh()",1000);
};

wpexpScrolling = {};
wpexpStartScrollNumber = function($e,to,suffix) {
	var idx = 0;
	var c = $e.css("color");
	if(!c) c = "#000";
	while(1) {
		idx = Math.random();
		if(!wpexpScrolling[idx]) {
			wpexpScrolling[idx] = $e;
			break;
		}
	}
	$e.css("color","#D1A425");
	wpexpScrollNumber(idx,to,suffix,c);
};

wpexpScrollNumber = function(idx,to,suffix,finish_color) {
	if(!wpexpScrolling[idx]) return;
	$e = wpexpScrolling[idx];
	cur = parseInt($e.text(),10);
	if(cur > to) {
		cur -= 1;
	} else if(cur < to) {
		cur += 1;
	} else {
		$e.animate({"color":finish_color},1000);
		delete wpexpScrolling[idx];
		return;
	}
	$e.text(cur+suffix);
	var time = 200/Math.abs(to - cur);
	window.setTimeout("wpexpScrollNumber("+idx+","+to+",'"+suffix+"','"+finish_color+"')",time);
};

wpexproFlashElmText = function($elm) {
	var c = $elm.css("color");
	if(!c) c = "#000";
	$elm.css("color","#D1A425").animate({"color":c},1000);
};

wpexproUpdateChart = function(start,end,check_first) {
	check_first = check_first === undefined ? false : check_first;
	data = {
		"action": "get_stats",
		"expid": wpexproData.id,
		"from": start,
		"to": end
	};
	jQuery.ajax({
		url: ajaxurl,
		data: data,
		success: function(d) {
			var total_convs = 0;
			if(d.exp) {
				for(var k in d.exp.tests) {
					var test = d.exp.tests[k];
					var cont = jQuery("[name=wpexpro-chart-data-"+test.id+"]");
					if(cont.length > 0) {
						$e = cont.find(".wpexpro-chart-data-conversions");
						total_convs += parseInt(test.conversions,10);
						if($e.text() != test.conversions) {
							$e.data("total",test.conversions);
							if(!wpexpChartHover)
								wpexpStartScrollNumber($e,test.conversions,"");
						}
						$e = cont.find(".wpexpro-chart-data-impressions");
						if($e.data("total") != test.impressions) {
							$e.data("total",test.impressions);
							if(!wpexpChartHover)
								wpexpStartScrollNumber($e,test.impressions,"");
						}
						if(test.impressions > 0) {
							test.percentage = Math.round(test.conversions/test.impressions*100);
							$e = cont.find(".wpexpro-chart-data-percentage");
							$e.data("total", test.percentage+"%");
							if(!wpexpChartHover)
								wpexpStartScrollNumber($e,test.percentage,"%");
						}

						if(test.probability) {
							$e = cont.find(".wpexpro-result");
							if($e.text() != test.probability+"%") {
								wpexpStartScrollNumber($e,test.probability,"%");
							}
						}
					}
				}
			}
			if(d.data) {
				//check if the chart looks different
				if(wpexproChartTimeLabels != d.data.labels.join("") || wpexproChartTotalConvs != total_convs) {
					wpexproChartStart = start;
					wpexproChartEnd = end;
					wpexproChartTotalConvs = total_convs;
					wpexproChartTimeLabels =d.data.labels.join("");
					wpexproDrawChart(d.data);
				}
				if(wpexproChartTimeTo == -1) wpexproChartRefresh();
			}
			if(d.status == "bad ts") {
				jQuery(".wpexpro-date-picker-cont input.wpexpro-date-picker-"+d.which).addClass("error");
				jQuery(".wpexpro-date-picker-cont").slideDown();
			}
		},
		dataType: 'json'
	});
};


// Close the Flash message on the admin page
wpexproCloseFlash = function(elm) {
	jQuery(elm).parent().fadeOut();
};

// Setup the original form to respond to the goal element focus
wpexproSetupOriginalForm = function($elm) {
	$elm.find(".wpexpro-exp-goal-type").change(function() {
		if(jQuery(this).val() == "0") {
			$elm.find(".wpexpro-exp-goal").show();
			$elm.find(".wpexpro-exp-goal-pv").hide();
			if($elm.find(".wpexpro-exp-goal").val()) {
				jQuery("[id^=wpexpro-goal-overlay-]",jQuery("iframe:visible").contents()).show();
			}
		} else {
			$elm.find(".wpexpro-exp-goal").hide();
			$elm.find(".wpexpro-exp-goal-pv").show();
			jQuery("[id^=wpexpro-goal-overlay-]",jQuery("iframe:visible").contents()).hide();
		}
	});

	// Test Case name changes
	$elm.find(".wpexpro-exp-goal").focus(function() {
		jQuery(this).addClass("wpexpro-exp-goal-focused");
		wpexproDoOriginalHover = true;
	}).blur(function() {
		jQuery(this).removeClass("wpexpro-exp-goal-focused");
		wpexproDoOriginalHover = false;
	});
};

// Setup the test case form to respond to changes to
// the test case name. It updates the case tab.
wpexproSetupTestCaseForm = function($elm) {
	// Test Case name changes
	$elm.find(".wpexpro-test-case-name").keyup(function() {
		var v = jQuery(this).val();
		if(!v) v = "New Test Case";
		jQuery(".nav-tab-active").text(v);
	});
};

// Submit the experiment editor form
wpexproDoSubmit = function() {
	var data = {
		title: "",
		description: "",
		status: "",
		goal_type: "",
		goal: "",
		tests: [],
		deleted_tests: wpexproDeletedTests
	};
	jQuery(".wpexpro-tab-pane").each(function(i,e) {
		$e = jQuery(e);

		if($e.attr("id") == "wpexpro-test-tmpl") return true;

		if($e.attr("id") == "wpexpro-exp-general") {
			//Save the general setting 
			$e.find("[name]").each(function(ni,ne){
				data[jQuery(ne).attr("name")] = jQuery(ne).val();
			});
		} else if($e.attr("id") == "wpex-original") {
			data['goal_type'] = $e.find(".wpexpro-exp-goal-type").val();
			if(data['goal_type'] == "0") {
				data['goal'] = $e.find(".wpexpro-exp-goal").val();
			} else {
				data['goal'] = $e.find(".wpexpro-exp-goal-pv").val();
			}
		} else {
			data["tests"].push( {
				id: $e.data("wpexpro-num"),
				name: $e.find(".wpexpro-test-case-name").val(),
				description: $e.find(".wpexpro-test-case-desc").val(),
				steps: _wpexproTestSteps[$e.attr("id")] || []
			});
		}
	});
	jQuery("#wpexpro-form input[name=data]").val(JSON.stringify(data));
	jQuery("#wpexpro-form").submit();
};

// Resize the iFrame when the window is resized
wpexproResizeVisibleIframe = function() {
	// Keep iframes as big as possible
	jQuery("iframe.wpexpro-exp").each(function(i,e){
		var o = jQuery(e).offset();
		jQuery(e).height(jQuery(window).height() - o.top-75);
		var $pane = jQuery(e).closest(".wpexpro-tab-pane");
		if($pane.attr("id") == "wpex-original" ){
			wpexproOriginalHighlightGoal(jQuery(e).contents());
		}
	});
};

wpexproChartWidth = 0;
wpexproDrawChart = function(data, resized) {
	if(resized === true) {
		//check if only the height changed
		if(wpexproChartWidth === jQuery("#wpexpro-chart").width()) return true;
	}
	jQuery("#wpexpro-chart").chart("clear");
	var d = {};

	if(data === undefined) {
		if(wpexproCurrentChartData) {
			data = wpexproCurrentChartData;
		} else {
			//initialize our time labels -- to tell if our chart changed
			if(wpexproChartData.labels) {
				wpexproChartTimeLabels = wpexproChartData.labels.join("");
			} else {
				wpexproChartTimeLabels = "";
			}
			data = wpexproChartData;
		}
	}

	wpexproCurrentChartData = data;

	var found_data = false;
	// do a deep-ish copy because elychart stomps on the config
	// object... grr....
	for(var k in data) {
		if(k == "values") {
			for(var k1 in data[k]) {
				if(data[k][k1].length > 0) {
					found_data = true;
					break;
				}
			}
		}
		d[k] = data[k];
	}
	// Only display the chart if there is something to show.
	if(found_data) {
		jQuery("#wpexpro-chart").html("");
		jQuery("#wpexpro-chart").removeClass("empty").chart(d);
		wpexproChartWidth = jQuery("#wpexpro-chart").width();
	} else {
		jQuery("#wpexpro-chart").html("<p class='alert'>No data is available to display.</p>").addClass("empty");
	}
};

wpexproInstallChartTemplate = function() {
	jQuery.elycharts.templates["line_basic_4"] = {
		type: "line",
		margins: [15, 75, 20, 75],
		normalize: true,
		defaultSeries: {
			rounded: 0,
			plotProps: {
				"stroke-width": 1
			},
			dot: true,
			dotProps: {
				stroke: "white",
				"stroke-width": 1,
				r: 3
			},
			tooltip: {
				active: false,
				padding: [0, 0],
				offset: [-30, -85],
				contentStyle: {
					"font-family": "Arial",
					"font-size": "10px",
					"line-height": "10px",
					color: "#888"
				},
				frameProps: {
					opacity: 0
				}
			},
			highlight: {
				newProps: {
					stroke: "white",
					r: 6
				}
			},
			startAnimation: {
				active: true,
				type: "avg",
				speed: 1000
			}
		},
		series: {
			serie1: {
				color: "#23819C"
			},
			serie2: {
				color: "#FF4848"
			},
			serie3: {
				color: "#59955C"
			},
			serie4: {
				color: "#FF62B0"
			},
			serie5: {
				color: "#3923D6"
			},
			serie6: {
				color: "#BC2EBC"
			}
		},
		defaultAxis: {
			labels: true,
			labelsDistance: 14,
			labelsHideCovered: true,
			labelsMarginRight: 10,
			stacked: false
		},
		axis: {
			l: {
				title: "Conversions",
				normalize: 1,
				titleDistance: tinymce.isIE ? 140 : 40
			},
			b: {
			}
		},
		features: {
			mousearea: {
				type: "index",
				opacity: 0.5,
				color: "#aaa",
				onMouseOver: wpexproChartMouseOver,
				onMouseOut: wpexproChartMouseOut
			},
			grid: {
				draw: [true, false],
				forceBorder: [true, false, true, false],
				props: {
					"stroke-dasharray": "-"
				}
			}
		}
	};
};
wpexpChartHover = false;
var wpexproChartMouseOut = function() {
	jQuery("[name=wpexpro-stat-title]").text("Current Conversion Totals");
	jQuery(".wpexpro-chart-data-data").each(function() {
		jQuery(this).text(jQuery(this).data("total"));
	});
	wpexpChartHover = false;
};

var wpexproChartMouseOver = function(env, serie, index, mouseAreaData) {
	var data = {};
	wpexpChartHover = true;
	for(var series in wpexproCurrentChartData._series) {
		var test_data = wpexproCurrentChartData._series[series][index];
		jQuery("[name=wpexpro-stat-title]").html("Conversion Totals: <span>"+ test_data.start_ts+" - "+test_data.end_ts+"</span>");

		var cont = jQuery("[name=wpexpro-chart-data-"+test_data.test_id+"]");
		cont.find(".wpexpro-chart-data-impressions").text(test_data.impressions);
		cont.find(".wpexpro-chart-data-conversions").text(test_data.conversions);
		var pcent = '';
		if(test_data.impressions !== 0) {
			pcent = Math.round((test_data.conversions/test_data.impressions)*100) + "%";
		}
		cont.find(".wpexpro-chart-data-percentage").text(pcent);
		data[test_data.test_id] = test_data;
	}
};

// From http://stackoverflow.com/a/4541963/1128407
var wpexproWaitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

