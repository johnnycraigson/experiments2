//{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
//{{{{                         Setup the test case editor                         }}}}
//{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

// This is called by the iframe when it is loaded
wpexproEnableIframe = function (iframeDoc) {
	// Remove the admin features
	jQuery("body",iframeDoc).removeClass("admin-bar");
	jQuery("#wpadminbar",iframeDoc).remove();
	jQuery("html",iframeDoc).attr("style","margin-top: 0 !important;");

	jQuery(iframeDoc).mouseout(function() {
		jQuery("#wpexpro-overlay-l, #wpexpro-overlay-r, #wpexpro-overlay-t, #wpexpro-overlay-b",this).hide();
	});

	jQuery("body",iframeDoc).append("<div id='wpexpro-overlay-l' style='display: none;'></div>");
	jQuery("body",iframeDoc).append("<div id='wpexpro-overlay-r' style='display: none;'></div>");
	jQuery("body",iframeDoc).append("<div id='wpexpro-overlay-t' style='display: none;'></div>");
	jQuery("body",iframeDoc).append("<div id='wpexpro-overlay-b' style='display: none;'></div>");
	jQuery("body",iframeDoc).append("<div id='wpexpro-overlay' style='display: none;'></div>");

	// If this is the original view, we need to set it up to
	// accept the goal clicks
	if(jQuery("#wpex-original").is(":visible")) {
		jQuery("body",iframeDoc).append("<div id='wpexpro-goal-overlay-l' style='display: none;'></div>");
		jQuery("body",iframeDoc).append("<div id='wpexpro-goal-overlay-r' style='display: none;'></div>");
		jQuery("body",iframeDoc).append("<div id='wpexpro-goal-overlay-t' style='display: none;'></div>");
		jQuery("body",iframeDoc).append("<div id='wpexpro-goal-overlay-b' style='display: none;'></div>");
		jQuery("body",iframeDoc).append("<div id='wpexpro-goal-overlay' style='display: none;'></div>");

		jQuery("body",iframeDoc).mousemove(function() {wpexproAddOverlay(iframeDoc,'#FF4848'); });
		wpexproSetupOriginalElements(jQuery("*",iframeDoc),iframeDoc);
	}else{
		jQuery("body",iframeDoc).mousemove(function() {wpexproAddOverlay(iframeDoc); });
		wpexproSetupElements(jQuery("*",iframeDoc),iframeDoc);
	}

	//Show it, now that's we've set it up
	jQuery(".wpexpro-tab-pane:visible div.wpexpro-loading").remove();

	// Get a script step we run it manually instead of wordpress putting it in
	// so that if it gets changed we don't run the old one and the new one
	var ss = wpexproGetScriptStep();
	if(ss !== null && ss.script) {
		var ifobj = jQuery(".wpexpro-tab-pane:visible iframe").get(0);
		var ifwin = ifobj.contentWindow || ifobj.contentDocument;
		ifwin.wpexproEval(ss.script);
	}

	jQuery(".wpexpro-tab-pane:visible iframe").show(0,wpexproResizeVisibleIframe);
};

// This adds a new test case to our experiment. This is
// called when the "New Case" tab is clicked or during
// initialization
wpexproAddTestCase = function(data, init, case_num) {
	var cur_num_tests = jQuery(".wpexpro-tab-pane:not(#wpexpro-exp-general):not(#wpexpro-test-tmpl)").length-2;
	if(wpexproTestLimit!=-1 && cur_num_tests == wpexproTestLimit) return;
	
	if(data === undefined) data = {name: '', description: ''};
	if(init === undefined) init = false;
	if(case_num === undefined) case_num = wpexproData.tests.length;

	var $tab = null;
	var id = case_num === 0 ? "wpex-original" : wpexproGenId();

	if(!init) {
		wpexproData.tests.push(data);
		// we added the tabs with our original markup to make it look like they
		// loaded instantly!
		$tab = jQuery('<a href="#'+id+'" class="wpexpro-tab nav-tab">'+(data.name ? data.name : 'New Test Case')+'</a>');
		$tab.insertBefore(jQuery("#wpexpro-add-test-case"));
	} else {
		jQuery("a[name=wpex-case-"+case_num+"]").attr("href","#"+id);
	}

	var $pane = jQuery("#wpexpro-test-tmpl").clone();

	$pane.attr("id",id);

	if(data.name) {
		$pane.find(".wpexpro-test-case-name").val(data.name);
	}
	if(data.description) {
		$pane.find(".wpexpro-test-case-desc").val(data.description);
	}
	if(data.id) {
		$pane.attr("data-wpexpro-num",data.id);
	}
	if(data.data && data.data.length > 0) {
		for(var i = 0; i < data.data.length; i++) {
			wpexproAddStep(data.data[i], $pane);
		}
	}

	$pane.find(".wpexpro-test-case-delete").click(wpexproDeleteCase);
	
	if(case_num === 0) {
		$pane.find(".wpexpro-exp-goal").val(wpexproData.goal);
		$pane.find(".wpexpro-test-case-name").closest("table").hide();
	} else {
		$pane.find(".wpexpro-exp-goal").closest("table").hide();
	}

	$pane.insertBefore(jQuery("#wpexpro-test-tmpl"));

	if(case_num !== 0) {
		wpexproSetupTestCaseForm($pane);
	} else {
		wpexproSetupOriginalForm($pane);
	}
	if(!init) {
		jQuery(".wpexpro-tab-pane").hide();
		$tab.trigger("click");
	}

	if(wpexproTestLimit!=-1 && cur_num_tests+1 == wpexproTestLimit) {
		jQuery(this).hide();
	}
};


// Delete a test case
wpexproDeleteCase = function(ev) {
	if(confirm("Are you sure you want to delete this test case?")) {
		var elm = jQuery(ev.target);
		var pane = jQuery(elm).closest(".wpexpro-tab-pane");
		var test_case_num = pane.data("wpexpro-num");
		wpexproDeletedTests.push(test_case_num);
		var tab = jQuery(".nav-tab[href=#"+pane.attr("id")+"]");
		tab.prev().click();
		tab.remove();
		pane.remove();
		var cur_num_tests = jQuery(".wpexpro-nav-tab-wrapper a.nav-tab:not(#wpexpro-add-test-case)").length-2;
		if(wpexproTestLimit!=-1 && cur_num_tests < wpexproTestLimit) {
			jQuery("#wpexpro-add-test-case").show();
		}
	}
};

// Setup the elements inside of a test case iFrame doc to respond
// to mouse events (hovering and clicking)
wpexproSetupElements = function($elms,iframeDoc) {
	$elms.css("cursor","pointer !important");
	$elms.hover(function(){
		if(!wpexproDoHover || jQuery(".reveal-modal-bg:visible").length > 0) return;
		if(jQuery(this).is("[id^=wpexpro-overlay]")) return;
		if(!jQuery(this).is(":visible")) return;
		jQuery(this).addClass("_wpexpro-mouseover");
		wpexproHighlightDeepest(iframeDoc);
	},function() {
		if(!wpexproDoHover || jQuery(".reveal-modal-bg:visible").length > 0) return;
		if(jQuery(this).is("[id^=wpexpro-overlay]")) return;
		if(!jQuery(this).is(":visible")) return;
		jQuery(".wpexpro-mouseover",iframeDoc).removeClass("_wpexpro-mouseover");
		wpexproHighlightDeepest(iframeDoc);
	}).click(function(ev){ wpexproHoverClick(ev,iframeDoc); return false; });
};

// Apply the stored steps of a test case
wpexproApplySteps = function(pane) {
	var $pane = pane === undefined ? jQuery(".wpexpro-tab-pane:visible") : pane;
	wpexproDoSteps(_wpexproTestSteps[$pane.attr("id")],$pane.find("iframe").contents());
};

// The Javascript implementation of the application
// of our test steps. This isn't actually used since it's
// all done in the wordpress backend
wpexproDoSteps = function(steps,iframeDoc) {
	for(var s in steps) {
		step = steps[s];
		switch(step.action) {
			case 'replace':
				try{
					wpexproGetElement(step.element,iframeDoc).replaceWith(step.content);
				}catch(err) {
					console.log("Error while applying step: "+ err, step);
				}
			break;
			case 'style':
				try{
					wpexproGetElement(step.element,iframeDoc).css(step.styles);
				}catch(err) {
					console.log("Error while applying step: "+ err, step);
				}
			break;
			case 'classes':
				try{
					wpexproGetElement(step.element,iframeDoc).attr("class","");
					wpexproGetElement(step.element,iframeDoc).addClass(step.classes.join(" "));
				}catch(err) {
					console.log("Error while applying step: "+ err, step);
				}
			break;
			case 'delete':
				try{
					wpexproGetElement(step.element,iframeDoc).remove();
				}catch(err) {
					console.log("Error while applying step: "+ err, step);
				}
			break;
		}
	}
};

//{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
//{{{{                        The In-between Functions                         }}}}
//{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

// Add a new step to the test. Called when the user changes something
wpexproGetScriptStep = function(pane) {
	var $pane = pane === undefined ? jQuery(".wpexpro-tab-pane:visible") : pane;
	if(_wpexproTestSteps[$pane.attr("id")] === undefined) return null;

	for(var i in _wpexproTestSteps[$pane.attr("id")]) {
		if(_wpexproTestSteps[$pane.attr("id")][i].action == "script") {
			return _wpexproTestSteps[$pane.attr("id")][i];
		}
	}
	return null;
};

wpexproAddStep = function(step,pane) {
	var $pane = pane === undefined ? jQuery(".wpexpro-tab-pane:visible") : pane;
	if(_wpexproTestSteps[$pane.attr("id")] === undefined) {
		_wpexproTestSteps[$pane.attr("id")] = [];
	}
	if(step.action == "script") {
		//if we are adding a script, we need to clear out the old one first
		for(var i in _wpexproTestSteps[$pane.attr("id")]) {
			if(_wpexproTestSteps[$pane.attr("id")][i].action == "script") {
				_wpexproTestSteps[$pane.attr("id")][i] = step;
				return;
			}
		}

	}
	_wpexproTestSteps[$pane.attr("id")].push(step);
};

// Update an element's content in the iFrame. Called when the
// content editor modal Save button is clicked.
wpexproUpdateIframeContent = function(src,$e,iframeDoc) {
	var new_html = "";
	if(src == "html") {
		new_html = wpexproCurrentHTMLEditor.getValue();
	} else {
		new_html = tinyMCE.activeEditor.getContent();
	}
	$e.html(new_html);
	wpexproAddOverlay(iframeDoc);
};

// This looks at all the elements that are currently being
// hovered over and finds the "deepest" one and adds
// the class to indicate it.
wpexproHighlightDeepest= function(iframeDoc) {
	jQuery(".wpexpro-mouseover",iframeDoc).removeClass("wpexpro-mouseover");
	jQuery("._wpexpro-mouseover:last",iframeDoc).addClass("wpexpro-mouseover");
};

wpexproGetOffset = function($e) {
	var totalOffset = {top: 0, left: 0};
	return totalOffset;
	// var $node = $e.offsetParent();
	// var $pnode = $e;
	// while($node.length && $pnode.get(0).tagName != "HTML") {
	// 		if($node.css("margin-top")) {
	// 			totalOffset.top += parseInt($node.css("margin-top"),10);
	// 			totalOffset.top += parseInt($node.css("padding-top"),10);
	// 			totalOffset.top += parseInt($node.css("border-top"),10);
	// 		}
	// 		if($node.css("margin-left")) {
	// 			totalOffset.left += parseInt($node.css("margin-left"),10);
	// 			totalOffset.left += parseInt($node.css("padding-left"),10);
	// 			totalOffset.left += parseInt($node.css("border-left"),10);
	// 		}
	// 	$pnode = $node;
	// 	$node = $node.offsetParent();
	// }
	// return totalOffset;
};

// Add overlay to the element when it is being hovered
wpexproAddOverlay = function(iframeDoc,color) {
	$e = jQuery(".wpexpro-mouseover",iframeDoc);
	if($e.length > 0) {
		//body is the offset parent of the overlay
		var eo = $e.offset();
		var ew = $e.width();
		var eh = $e.height();

		var ept = parseInt($e.css("padding-top"),10);
		var epb = parseInt($e.css("padding-bottom"),10);
		var epl = parseInt($e.css("padding-left"),10);
		var epr = parseInt($e.css("padding-right"),10);

		eh += ept + epb;
		ew += epr + epl;
		
		if(eh === 0 || ew === 0) return;
		
		//Get all the elements offsets based on margin and border
		var totalOffset = wpexproGetOffset($e);

		jQuery("#wpexpro-overlay-l",iframeDoc).css({
			top: eo.top-wpexproOutlineOffset-totalOffset.top,
			left: eo.left-wpexproOutlineOffset,
			height: eh+2+2*wpexproOutlineOffset,
			width: '2px'
		}).show();
		jQuery("#wpexpro-overlay-r",iframeDoc).css({
			top: eo.top-wpexproOutlineOffset-totalOffset.top,
			left: eo.left - (wpexproOutlineOffset) + ew + 2*wpexproOutlineOffset,
			height: eh+2+2*wpexproOutlineOffset,
			width: '2px'
		}).show();
		jQuery("#wpexpro-overlay-t",iframeDoc).css({
			top: eo.top-wpexproOutlineOffset-totalOffset.top,
			left: eo.left-wpexproOutlineOffset,
			height: '2px',
			width: ew + 2*wpexproOutlineOffset
		}).show();
		jQuery("#wpexpro-overlay-b",iframeDoc).css({
			top: eo.top - wpexproOutlineOffset-totalOffset.top + eh + 2*wpexproOutlineOffset,
			left: eo.left - wpexproOutlineOffset,
			height: '2px',
			width: ew + 2*wpexproOutlineOffset
		}).show();

		if(color !== undefined) {
			jQuery("#wpexpro-overlay-l, #wpexpro-overlay-r, #wpexpro-overlay-t, #wpexpro-overlay-b",iframeDoc).css("background-color",color);
		}
	} else {
		jQuery("#wpexpro-overlay-l, #wpexpro-overlay-r, #wpexpro-overlay-t, #wpexpro-overlay-b",iframeDoc).hide();
	}
};

// This is called when the hover overlay is clicked
wpexproHoverClick = function(ev,iframeDoc) {
	//If the menu is being shown, clear it
	$menu = jQuery("#wpexpro-menu",iframeDoc);
	if($menu.length > 0) {
		$menu.remove();
		jQuery("#wpexpro-overlay",iframeDoc).hide();
		wpexproDoHover = true;
		return false;
	}

	$e = jQuery(ev.target);
	if(!$e.hasClass("wpexpro-mouseover")) return false;
	ev.preventDefault();
	ev.stopPropagation();

	wpexproDoHover = false;

	var bodyoffset = jQuery("body").offset();

	var eo = $e.offset();
	var ew = $e.width();
	var eh = $e.height();
	var ept = parseInt($e.css("padding-top"),10);
	var epb = parseInt($e.css("padding-bottom"),10);
	var epl = parseInt($e.css("padding-left"),10);
	var epr = parseInt($e.css("padding-right"),10);

	//Get all the elements offsets based on margin and border
	var totalOffset = wpexproGetOffset($e);

	eh += ept + epb;
	ew += epr + epl;

	if(eh === 0 || ew === 0) return false;
	
	jQuery("#wpexpro-overlay",iframeDoc).css({
		top: eo.top - wpexproOutlineOffset-totalOffset.top,
		left: eo.left - wpexproOutlineOffset,
		height: eh + 2*wpexproOutlineOffset,
		width: ew + 2*wpexproOutlineOffset
	}).show();

	$menu = jQuery("#wpexpro-menu").clone();
	$menu.css({
		top: ev.pageY-totalOffset.top,
		left: ev.pageX
	});
	jQuery("body",iframeDoc).append($menu);

	$menu.find("a.wpexpro-add-script").click(function(ev){ wpexproEditScript(ev,iframeDoc); });
	$menu.find("a.wpexpro-delete-elm").click(function(ev){ wpexproDeleteElm(ev,iframeDoc); });
	$menu.find("a.wpexpro-html-editor").click(function(ev){ wpexproEditContent(ev,iframeDoc); });
	$menu.find("a.wpexpro-class-editor").click(function(ev){ wpexproEditClasses(ev,iframeDoc); });
	$menu.find("a.wpexpro-style-editor").click(function(ev){ wpexproEditStyle(ev,iframeDoc); });
	$menu.show();

	return false;
};

//{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}
//{{{{                         The Actual Step Functions                        }}}}
//{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

// Add a delete element step to this test case
wpexproDeleteElm = function(ev,iframeDoc) {
	ev.preventDefault();
	ev.stopPropagation();
	var $e = jQuery(".wpexpro-mouseover",iframeDoc);
	if($e.length > 0) {
		wpexproAddStep({
			action: "delete",
			element: wpexproGetSelector($e)
		});
		$e.remove();
	}
	//This clears the menu
	wpexproHoverClick(ev,iframeDoc);
	jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
};

// Add an edit style step to this test case
wpexproEditStyle = function(ev,iframeDoc) {
	ev.preventDefault();
	ev.stopPropagation();
	var $e = jQuery(".wpexpro-mouseover",iframeDoc);
	if($e.length > 0) {

		// Clone our editor modal box, set the html, add it
		// to our body.
		$m = jQuery("#wpexpro-style-editor").clone();
		$m.removeAttr("id");
		jQuery("body").append($m);

		var orig_styles = wpexproCSSToObj($e.get(0).style);

		var  _wpexproAdjustStyles = function(original) {
			$e.attr("style","");
			if(original === true) {
				$e.css(orig_styles);
				wpexproAddOverlay(iframeDoc);
				return null;
			} else {
				var _styles = {};
				var _cinps = $m.find(".wpexpro-styles-tbl .wpexpro-style-edit-wrap");
				_cinps.each(function(i,e){
					var sk = jQuery.trim(jQuery(e).children(".wpexpro-style-edit-key").val());
					var sv = jQuery.trim(jQuery(e).children(".wpexpro-style-edit-val").val());
					if(sk.length > 0 && sv.length > 0)
						_styles[sk] = sv;
				});
				$e.css(_styles);
				wpexproAddOverlay(iframeDoc);
				return _styles;
			}
		};

		var ctbl = $m.find(".wpexpro-styles-tbl");
		$row_tmpl = jQuery("<div class='wpexpro-style-edit-wrap'><input class='wpexpro-style-edit-key' value='' placeholder='style-key' /><input class='wpexpro-style-edit-val' value='' placeholder='style value' /><div class='wpexpro-style-edit-rm'>x</div></div>");
		for(var k in orig_styles) {
				$rowc = $row_tmpl.clone();
				$rowc.children("input").keyup(_wpexproAdjustStyles);
				$rowc.find("input.wpexpro-style-edit-key").val(k);
				$rowc.find("input.wpexpro-style-edit-val").val(orig_styles[k]);
				ctbl.append($rowc);
		}

		ctbl.on("click",".wpexpro-style-edit-rm",function() {
			jQuery(this).closest(".wpexpro-style-edit-wrap").remove();
			// _wpexproAdjustStyles();
			return false;
		});

		$m.find(".wpexpro-add-style").click(function() {
			$rowc = $row_tmpl.clone();
			// $rowc.children("input").keyup(_wpexproAdjustStyles);
			ctbl.append($rowc);
			return false;
		});

		// Add our close button functionality
		$m.find("a.wpexpro-close-btn").click(function() {
			// Make sure our iframe is updated
			_wpexproAdjustStyles(true);

			$m.find(".close-reveal-modal").click();
			$m.remove();
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});

		// Add our save button functionality
		$m.find("a.wpexpro-save-btn").click(function() {
			// Make sure our iframe is updated
			var new_styles = _wpexproAdjustStyles();

			if(new_styles) {
				wpexproAddStep({
					action: "style",
					element: wpexproGetSelector($e),
					styles: new_styles
				});
			}

			$m.find(".close-reveal-modal").click();
			$m.remove();
			// Remove our outline
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});

		// All Done! Open up the modal
		$m.reveal({
			closeonbackgroundclick: false
		});
		$m.on("click", ".close-reveal-modal", function() {
			wpexproCurrentHTMLEditor = null;
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
			$m.remove();
		});
	}

	wpexproHoverClick(ev,iframeDoc);
};

// Add custom javascript
wpexproEditScript = function(ev,iframeDoc) {
	ev.preventDefault();
	ev.stopPropagation();
	var $e = jQuery(".wpexpro-mouseover",iframeDoc);
	if($e.length > 0) {

		// Clone our editor modal box, set the html, add it
		// to our body.
		$m = jQuery("#wpexpro-script-editor").clone();
		$m.removeAttr("id");
		jQuery("body").append($m);

		//Set up our code mirror editor
		wpexproCurrentHTMLEditor = CodeMirror.fromTextArea($m.find(".wpexpro-script > textarea").get(0), {mode: "javascript", tabMode: "indent",lineWrapping: true,lineNumbers:true});
		var p = wpexproGetScriptStep();
		if(p !== null) {
			wpexproCurrentHTMLEditor.setValue(p.script);
		}

		// Add our close button functionality
		$m.find("a.wpexpro-close-btn").click(function() {
			$m.find(".close-reveal-modal").click();
			$m.remove();
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});

		// Add our save button functionality
		$m.find("a.wpexpro-save-btn").click(function() {
			wpexproAddStep({
				action: "script",
				element: "script",
				script: wpexproCurrentHTMLEditor.getValue()
			});

			$m.find(".close-reveal-modal").click();
			$m.remove();
			iframeDoc.location.reload();
		});

		// All Done! Open up the modal
		$m.reveal({
			closeonbackgroundclick: false
		});
		$m.on("click", ".close-reveal-modal", function() {
			wpexproCurrentHTMLEditor = null;
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
			$m.remove();
		});
	}

	wpexproHoverClick(ev,iframeDoc);
};

// Add an edit classes step to this test case
wpexproEditClasses = function(ev,iframeDoc) {
	ev.preventDefault();
	ev.stopPropagation();
	var $e = jQuery(".wpexpro-mouseover",iframeDoc);
	if($e.length > 0) {
		// Clone our editor modal box, set the html, add it
		// to our body.
		$m = jQuery("#wpexpro-class-editor").clone();
		$m.removeAttr("id");
		jQuery("body").append($m);

		var orig_class_str = $e.attr("class");
		var orig_classes = orig_class_str.split(/\s+/);

		var  _wpexproAdjustClasses = function(original) {
			if(original === true) {
				$e.attr("class",orig_classes.join(" "));
				wpexproAddOverlay(iframeDoc);
				return null;
			} else {
				var _cinps = $m.find(".wpexpro-classes-tbl input");
				var _classes = ["wpexpro-mouseover","_wpexpro-mouseover"];
				_cinps.each(function(i,e){
					_classes.push(jQuery(e).val());
				});
				$e.attr("class",_classes.join(" "));
				wpexproAddOverlay(iframeDoc);
				return _classes.splice(2); //ignore the first two that we added
			}
		};

		var ctbl = $m.find(".wpexpro-classes-tbl");
		var skip = ["wpexpro-mouseover","_wpexpro-mouseover"];
		$row_tmpl = jQuery("<div class='wpexpro-class-edit-wrap'><input class='wpexpro-class-edit' value='' placeholder='class-name' /><div class='wpexpro-class-edit-rm'>x</div></div>");
		for(var i in orig_classes) {
			if(skip.indexOf(orig_classes[i]) == -1) {
				$rowc = $row_tmpl.clone();
				$rowc.children("input").keyup(_wpexproAdjustClasses);
				$rowc.find("input").val(orig_classes[i]);
				ctbl.append($rowc);
			}
		}
		ctbl.on("click",".wpexpro-class-edit-rm",function(){
			jQuery(this).closest(".wpexpro-class-edit-wrap").remove();
			// _wpexproAdjustClasses();
			return false;
		});

		$m.find(".wpexpro-add-class").click(function() {
			$rowc = $row_tmpl.clone();
			// $rowc.children("input").keyup(_wpexproAdjustClasses);
			ctbl.append($rowc);
			return false;
		});

		// Add our close button functionality
		$m.find("a.wpexpro-close-btn").click(function() {
			// Make sure our iframe is updated
			_wpexproAdjustClasses(true);

			$m.find(".close-reveal-modal").click();
			$m.remove();
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});

		// Add our save button functionality
		$m.find("a.wpexpro-save-btn").click(function() {
			// Make sure our iframe is updated
			var new_classes = _wpexproAdjustClasses();
			if(new_classes) {
				wpexproAddStep({
					action: "classes",
					element: wpexproGetSelector($e),
					classes: new_classes
				});
			}

			$m.find(".close-reveal-modal").click();
			$m.remove();
			// Remove our outline
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});


		// All Done! Open up the modal
		$m.reveal({
			closeonbackgroundclick: false
		});
		$m.on("click", ".close-reveal-modal", function() {
			wpexproCurrentHTMLEditor = null;
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
			$m.remove();
		});
	}
	wpexproHoverClick(ev,iframeDoc);
};

// Add an edit content step to this test case
wpexproEditContent = function(ev,iframeDoc) {
	ev.preventDefault();
	ev.stopPropagation();
	var $e = jQuery(".wpexpro-mouseover",iframeDoc);
	if($e.length > 0) {

		// Make a clone so we don't mess up the orginal element and remove our classes
		$eclone = $e.clone();
		$eclone.removeClass("wpexpro-mouseover _wpexpro-mouseover");
		$eclone.find("._wpexpro-mouseover").removeClass("wpexpro-mouseover _wpexpro-mouseover");

		var orig_selector = wpexproGetSelector($e);

		//Wrap the element in a div tag and grab it's html (this allows
		//	us to get the html of the current element as well), then pretty
		//  up the html and ditch the clone
		var orig_html = $eclone.wrap("<div />").parent().html();
		orig_html = orig_html.replace(/>\s*</g,">\n<");
		$eclone.remove();

		// Clone our editor modal box, set the html, add it
		// to our body.
		$m = jQuery("#wpexpro-html-editor").clone();
		$m.removeAttr("id");
		$m.find("textarea").val(orig_html);
		jQuery("body").append($m);

		// Add our close button functionality
		$m.find("a.wpexpro-close-btn").click(function() {
			$s = jQuery(orig_html);
			$e.replaceWith($s);
			var elms = jQuery("*",$s);
			elms.push($s[0]);
			wpexproSetupElements(elms,iframeDoc);

			$m.find(".close-reveal-modal").click();
			$m.remove();
			wpexproCurrentHTMLEditor = null;
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});

		// Add our save button functionality
		$m.find("a.wpexpro-save-btn").click(function() {
			//Grab the current editors text content
			var s = "";
			if($m.find("li.wpexpro-editor-visual-tab").hasClass("nav-tab-active")) {
				s = tinyMCE.activeEditor.getContent();
				wpexproUpdateIframeContent("editor",$e,iframeDoc);
			} else {
				s = wpexproCurrentHTMLEditor.getValue();
				wpexproUpdateIframeContent("html",$e,iframeDoc);
			}

			// Try to jQuery-fi the returned content
			// if it failes, try wrapping it in a span and try
			// again. This case is when the user deletes everything
			// and types plain text.
			// If it still fails - tell the user
			try {
				$s = jQuery(s);
				if($s.length === 0) {
					$s = jQuery("<span>"+s+"</span>");
				}
			} catch(err) {
				try {
					$s = jQuery("<span>"+s+"</span>");
				} catch(err2) {
					alert("There seems to be an error in your HTML");
					return;
				}
			}

			$sclone = $s.clone();
			wpexproAddStep({
				action: "replace",
				element: wpexproGetSelector($e),
				content: $sclone.wrap("<div />").parent().html()
			});
			$sclone.remove(); //be a good citizen

			//Replace our original element with this one
			$e.replaceWith($s);

			// Get all of the elements in our new content. We need
			// to make them magical again
			var elms = jQuery("*",$s);

			// while $s started out as a top level wrapper
			// we cannot gaurentee that the user didn't add
			// more top level nodes
			for(var k=0;k<$s.length; k++) {
				elms.push($s[k]);
			}

			// Do the magic
			wpexproSetupElements(elms,iframeDoc);

			// Close and remove the modal
			$m.find(".close-reveal-modal").click();
			$m.remove();

			// Clear the editor
			wpexproCurrentHTMLEditor = null;

			// Remove our outline
			jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
		});

		//Set up our code mirror editor
		var mixedMode = {
			name: "htmlmixed",
			scriptTypes: [{
				matches: /\/x-handlebars-template|\/x-mustache/i,
				mode: null
			},{
				matches: /(text|application)\/(x-)?vb(a|script)/i,
				mode: "vbscript"
			}]
		};
		wpexproCurrentHTMLEditor = CodeMirror.fromTextArea($m.find(".wpexpro-html > textarea").get(0), {mode: mixedMode, tabMode: "indent",lineWrapping: true,lineNumbers:true});

		// Indent all the lines
		for (var i = 0, e = wpexproCurrentHTMLEditor.lineCount(); i < e; ++i) wpexproCurrentHTMLEditor.indentLine(i);

		// Keep the iframe doc in sync with what we edit in the HTML editor
		// wpexproCurrentHTMLEditor.on("change", function() {
		// wpexproUpdateIframeContent("html",$e,iframeDoc);
		// });

		// Set up our tinymce editor
		var ts = new Date().getTime();
		// set the ids based on our time (gives us some level of uniquness)
		$m.find(".wpexpro-editor > textarea").attr("id","wpexpro-editor-"+ts);
		$m.find(".wp-media-buttons>a").attr("data-editor","wpexpro-editor-"+ts);
		
		if(typeof(wpexproTinyMCESettings['external_plugins']) != "object") {
			wpexproTinyMCESettings['external_plugins'] = jQuery.parseJSON(wpexproTinyMCESettings['external_plugins']);
		}
		
		tinymce.init( jQuery.extend(wpexproTinyMCESettings,{
			forced_root_block : false,
			force_p_newlines : false,
			remove_linebreaks : false,
			force_br_newlines : false,
			remove_trailing_nbsp : false,
			verify_html : false,
			init_instance_callback: function() {
				// this resets some set inside of wordpress' media button code
				wpActiveEditor = tinyMCE.activeEditor;

				//Make the tinymce editor the correct height		
				$m.find("[id$=_ifr]").height(250);

				// Updat the iframe when they change the content
				//wpActiveEditor.onKeyUp.add(function(ed, l) {
					//wpexproUpdateIframeContent("editor",$e,iframeDoc);
				//});

				// Set the active tab (this gets removed on init)
				$m.find("li.wpexpro-editor-visual-tab").addClass("nav-tab-active");

				//Change to HTML editor
				$m.find("li.wpexpro-editor-text-tab").click(function() {
					// show and hide
					$m.find(".wpexpro-editor").hide();
					$m.find(".wpexpro-media").hide();
					$m.find(".wpexpro-html").show();

					var new_html = tinyMCE.activeEditor.getContent().replace(/>\s*</g,">\n<");
					wpexproCurrentHTMLEditor.setValue(new_html);
					// Indent all the lines
					for (var i = 0, e = wpexproCurrentHTMLEditor.lineCount(); i < e; ++i) wpexproCurrentHTMLEditor.indentLine(i);

					// mark the tab
					$m.find(".wpexpro-editor-text-tab").addClass("nav-tab-active");
					$m.find(".wpexpro-editor-visual-tab").removeClass("nav-tab-active");
				});

				//Change to visual editor
				$m.find("li.wpexpro-editor-visual-tab").click(function() {
					// show and hide
					$m.find(".wpexpro-editor").show();
					$m.find(".wpexpro-media").show();
					$m.find(".wpexpro-html").hide();

					var new_html = wpexproCurrentHTMLEditor.getValue().replace(/>\s*</g,">\n<");
					tinyMCE.activeEditor.setContent(new_html);

					// mark the tab
					$m.find(".wpexpro-editor-text-tab").removeClass("nav-tab-active");
					$m.find(".wpexpro-editor-visual-tab").addClass("nav-tab-active");
				});

				// All Done! Open up the modal
				$m.reveal({
					closeonbackgroundclick: false
				});

				$m.on("click", ".close-reveal-modal", function() {
					wpexproCurrentHTMLEditor = null;
					jQuery("#wpexpro-overlay-l,#wpexpro-overlay-r,#wpexpro-overlay-t,#wpexpro-overlay-b",iframeDoc).hide();
					$m.remove();
				});
			}
		}));
		tinymce.execCommand('mceAddEditor',true,"wpexpro-editor-"+ts);
	}

	wpexproHoverClick(ev,iframeDoc);
};