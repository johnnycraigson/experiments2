<?php 
	if(isset($_REQUEST['save-license'])) {
		update_option("wpexpro__license",$_REQUEST['license']);
		//force license check
		update_option("wpexpro__license_check",current_time('timestamp')-12*60*60-60);
		$ret = $wpexpro_obj->do_license_check();
		if($ret === TRUE) {
			$wpexpro_obj->redirect_to($wpexpro_obj->pages['index'], "License validated.");
		}
		global $license_fail_reason;
		$license_fail_reason = $wpexpro_obj->translateLicenseFail($ret);
	}
	if(!$wpexpro_obj->valid_license) {
		$license = get_option("wpexpro__license");
		require dirname(__FILE__).'/wpexpro-license-settings.php';
		exit();
	}
	$exps = $wpexpro_obj->getExperiments(isset($_REQUEST['filter']) ? $_REQUEST['filter'] : NULL);
?>

<div class="wrap">
	<div id="icon-themes" class="icon32"><br></div>
	<h2>Experiments <a href="<?php echo $wpexpro_obj->pages['new']; ?>" class="add-new-h2">Add New</a></h2>
	
	<ul class="subsubsub">
		<li><a href="<?php echo $wpexpro_obj->pages['index']; ?>" class="<?php echo $wpexpro_obj->currentSubSub(''); ?>">All <span class="count">(<?php echo $wpexpro_obj->countExps(); ?>)</span></a></li>
		<?php foreach($wpexpro_obj->STATUS_STR as $stat=>$str): ?>
			<?php $stat_count = $wpexpro_obj->countExpsByStatus($stat); ?>
			<?php if($stat_count > 0): ?>
				<li> | <a href="<?php echo $wpexpro_obj->pages['index']; ?>&amp;filter%5Bstatus%5D=<?php echo $stat; ?>" class="<?php echo $wpexpro_obj->currentSubSub($stat); ?>"><?php wpexpro_ehe($str); ?> <span class="count">(<?php echo $stat_count; ?>)</span></a></li>
			<?php endif; ?>
		<?php endforeach; ?>
	</ul>
	
	<table class="wp-list-table widefat fixed posts" cellspacing="0">
		<thead>
			<tr>
				<th class="manage-column column-cb check-column" style="">
					<label class="screen-reader-text" for="cb-select-all-1">Select All</label><input id="cb-select-all-1" type="checkbox">
				</th>
				<th>
					<a href="http://wp-experiments-test.dev/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Title</span><span class="sorting-indicator"></span></a>
				</th>
				<th>Page</th>
				<th>Creator</th>
				<th>Status</th>
				<th scope="col" id="tags" class="manage-column column-tags" style=""># Tests</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th class="manage-column column-cb check-column" style="">
					<label class="screen-reader-text" for="cb-select-all-1">Select All</label><input id="cb-select-all-1" type="checkbox">
				</th>
				<th class="manage-column column-title sortable desc" style="">
					<a href="http://wp-experiments-test.dev/wp-admin/edit.php?orderby=title&amp;order=asc"><span>Title</span><span class="sorting-indicator"></span></a>
				</th>
				<th scope="col" id="categories" class="manage-column column-categories" style="">Page</th>
				<th scope="col" id="author" class="manage-column column-author" style="">Creator</th>
				<th scope="col" id="tags" class="manage-column column-tags" style="">Status</th>
				<th scope="col" id="tags" class="manage-column column-tags" style=""># Tests</th>
			</tr>
		</tfoot>
		<tbody id="the-list">
			<?php if(count($exps) == 0): ?>
				<tr valign="top">
					<th scope="row" class="check-column">
					</th>
					<td colspan="5">
						<p class="alert">There are no experiments here. <a href="<?php echo $wpexpro_obj->pages['new']; ?>">Why don't you create one?</a></p>
					</td>
				</tr>
			<?php else: ?>
				<?php foreach($exps as $exp): ?>
					<tr id="wpexpro-<?php echo $exp['id']; ?>"> 
						<th scope="row" class="check-column">
							<input type="checkbox" name="experiment[]" value="<?php echo $exp['id']; ?>">
						</th>
						<td class="post-title page-title column-title">
							<strong>
								<a class="row-title" href="<?php echo $wpexpro_obj->pages['edit'] ?><?php echo $exp['id']; ?>" title="Edit '<?php wpexpro_ehe($exp['title']); ?>'"><?php wpexpro_ehe($exp['title']); ?></a>
							</strong>
							<div class="row-actions">
								<span class="edit"><a href="<?php echo $wpexpro_obj->pages['edit'] ?><?php echo $exp['id']; ?>" title="Edit '<?php wpexpro_ehe($exp['title']); ?>'">Edit</a> | </span>
								<span class="trash"><a onclick='return confirm("Are you sure you want to delete this experiment and all of its data?");' class="submitdelete" title="Delete this experiment" href="<?php echo $wpexpro_obj->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=trash">Delete</a></span>
							</div>
						</td>
						<td class="post-title page-title column-title">
							<?php wpexpro_ehe($wpexpro_obj->getPostTitle($exp['post_id'])); ?>
						</td>
						<td class="post-title page-title column-title">
							<?php wpexpro_ehe($wpexpro_obj->getUserName($exp['user_id'])); ?>
						</td>
						<td class="post-title page-title column-title">
							<?php wpexpro_ehe($wpexpro_obj->translateStatus($exp['status'])); ?>
						</td>
						<td class="post-title page-title column-title">
							<?php wpexpro_ehe($wpexpro_obj->countTests($exp['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
</div>