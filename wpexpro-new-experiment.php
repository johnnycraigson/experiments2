<?php
	list($posts,$pages,$inc_root) = $this->getPosts();
?>

<div class="wrap">
	<div id="icon-edit-pages" class="icon32 icon32-posts-page"><br></div>
	<h2>New Experiment</h2>
	<form method="post">
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row"><label for="blogname">Experiment Title</label></th>
					<td>
						<input name="experiment[title]" type="text" value="<?php wpexpro_ehe($exp['title']); ?>" class="regular-text">
						<?php if(isset($errors['title'])):?><p class="description error"><?php wpexpro_ehe($errors['title']); ?></p><?php endif; ?>
						<p class="description">A short and sweet title for this experiment.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Experiment Description</label></th>
					<td>
						<textarea class="regular-text" rows="5" name="experiment[description]" ><?php wpexpro_ehe($exp['description']); ?></textarea>
						<?php if(isset($errors['description'])):?><p class="description error"><?php wpexpro_ehe($errors['description']); ?></p><?php endif; ?>
						<p class="description">A longer but still sweet description for this experiment.</p>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Experiment Page</label></th>
					<td>
						<select name="experiment[post_id]" >
							<option value="">Choose a Page...</option>
							<?php if($inc_root): ?>
								<option value="root" <?php if($exp['goal'] == "root") echo 'selected'; ?>>Home Page</option>
							<?php endif; ?>
							<?php if(count($pages)): ?>
								<optgroup label="Pages">
									<?php foreach($pages as $page): ?>
										<option value="<?php echo $page->ID; ?>" <?php if($exp['post_id'] == $page->ID) echo 'selected'; ?>><?php echo $page->post_title; ?></option>
									<?php endforeach; ?>
								</optgroup>
							<?php endif; ?>

							<?php if(count($posts)): ?>
								<optgroup label="Posts">
									<?php foreach($posts as $post): ?>
										<option value="<?php echo $post->ID; ?>" <?php if($exp['post_id'] == $post->ID) echo 'selected'; ?>><?php echo $post->post_title; ?></option>
									<?php endforeach; ?>
								</optgroup>
							<?php endif; ?>
						</select>
						<?php if(isset($errors['post_id'])):?><p class="description error"><?php wpexpro_ehe($errors['post_id']); ?></p><?php endif; ?>
						<p class="description">The page on which you are going to experiment.</p>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="submit">
			<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
		</p>
	</form>
</div>
