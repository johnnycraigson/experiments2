<?php
/**
 * This is the main class for WP Experiemnts
 */

class WPExPro {
	private $exp_tbl;
	private $test_tbl;
	private $stat_tbl;

	private $now;
	public $valid_license;

	//Used when we are doing the simpson integral
	//approximation to determine the probablity
	//of a test being displayed
	private $statTests;
	private $statChecking;

	private $role; //the current user's role
	private $exps;
	private $use_bucket;
	public $pages = array(
		'index' => "/wp-admin/admin.php?page=wp-experiments/wpexpro-admin-menu.php",
		"edit" => "/wp-admin/admin.php?page=wpexpro-edit-experiment&exid=",
		'new' => "/wp-admin/admin.php?page=wpexpro-new-experiment"
	);

	const GOAL_CLICK = 0;
	const GOAL_PAGEVIEW = 1;

	const STATUS_NEW = 0;
	const STATUS_READY = 1;
	const STATUS_RUNNING= 2;
	const STATUS_PAUSED= 3;
	const STATUS_STOPPED= 4;

	public $STATUS_STR = array(
		self::STATUS_NEW=>"New",
		self::STATUS_READY=>"Ready",
		self::STATUS_RUNNING=>"Running", 
		self::STATUS_PAUSED=>"Paused",
		self::STATUS_STOPPED=>"Stopped");


	function __construct() {
		global $wpdb, $current_user, $wp_roles;;
		$this->now = current_time('timestamp', 1);
		if (!session_id()) session_start();
		//Admin CSS
		add_action('admin_enqueue_scripts',array($this,'admin_enqueue'));
		
		//Handle flash
		add_action( 'admin_notices', array($this, 'display_flash'));

		if (!$this->do_license_check()) {
			$this->valid_license = false;
			//Setup admin menu
			add_action( 'admin_menu', array($this,'register_license_menu' ));
			return;
		} else {
			$this->valid_license = true;
		}

		$this->exp_tbl = $wpdb->prefix . "wpexpro_exps";
		$this->test_tbl = $wpdb->prefix . "wpexpro_tests";
		$this->stat_tbl = $wpdb->prefix . "wpexpro_stats";
		$this->exps = array();
		
		$this->use_bucket = get_option("wpexpro__use_bucket");
		//get_option return FALSE if the option doesn't exist
		if($this->use_bucket === FALSE) $this->use_bucket = 1;
		//Setup admin menu
		add_action( 'admin_menu', array($this,'register_menu' ));
		
		add_action('http_api_curl', array($this, 'add_ca_bundle' ));

		//Normal Include Scripts
		add_action('wp_enqueue_scripts',array($this,'enqueue'));

		//Change the content
		add_action('wp', array($this, 'setup_output_buffering'));
		add_action('wp', array($this, 'check_pageview_goals'));
		// add_action('shutdown', array($this, 'finish_output_buffering'));

		add_action('wp_ajax_trigger_check', array($this,'record_trigger_check'));
		add_action('wp_ajax_nopriv_trigger_check', array($this,'record_trigger_check'));
		add_action('wp_ajax_get_stats', array($this,'ajax_get_stats'));

		add_action('wp_head',array($this,'add_ajaxurl'));
	}

	function add_ca_bundle(&$ch) {
		global $api_url;
		$info = curl_getinfo($ch);

		// Set our SSL CA Bundle if it's our connection 
		if(stristr($info['url'], "wpexperiments.com/api/") !== FALSE) {
			curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__)."/ssl.ca-bundle");
		}
	}
	
	// Hello you. While you could obviously disable this function
	// to get around the license check, I would encourage you not
	// to do so. I worked hard on this plugin and deserve the price
	// of the plugin. If you don't think I do, then write your own.
	// Don't just take mine. You might think that a car is overpriced,
	// that doesn't justify you stealing it.
	function do_license_check() {
		global $_FREE_TRIAL_LENGTH;
		// check once every 12 hours
		$license = get_option("wpexpro__license");
		$last_check = get_option("wpexpro__license_check");
		$valid = get_option("wpexpro__valid_license");
		$fails = get_option("wpexpro__api_failures");
		$url = get_option("wpexpro__api_url");
		
		if(strlen(trim($license))==0) {
			$started = get_option("wpexpro__installed"); 
			$current = current_time("timestamp", 1);
			$remaining = ($_FREE_TRIAL_LENGTH*24*60*60) - ($current - $started);
			if($remaining > 0) {
				update_option("wpexpro__num_sites",1);
				update_option("wpexpro__num_exps",1);
				update_option("wpexpro__num_tests",2);
				return true;
			}
			return false;
		} 

		if($last_check === FALSE || $last_check < ($this->now - 12*60*60)) {
			$data = array (
				"action"=>"check",
				"site_url"=>get_site_url(),
				"license"=>$license
			);

			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,CURLOPT_POST, count($data));
			curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($ch,CURLOPT_CAINFO, dirname(__FILE__)."/ssl.ca-bundle");
			$result = curl_exec($ch);

			$jresult = json_decode($result,true);
			if($jresult === FALSE || $jresult['status'] == "error") {
				if($fails === FALSE) {
					//this is our first api failure
					get_option("wpexpro__api_failures",1);
				}
				$fails += 1;
				if($fails < 4) {
					update_option("wpexpro__api_failures",$fails);
				} else {
					$valid = false;
					update_option("wpexpro__valid_license",$valid);
					update_option("wpexpro__api_failures",0);
				}
				// lets try again in 30 minutes since we didn't connect to the api
				update_option("wpexpro__license_check",$this->now-12*60*60+60*30);

				if($jresult === FALSE ) {
					$che = curl_errno($ch);
					curl_close($ch);
	
					if($valid === FALSE) return "api-error-".$che;	
				} else {
					return $jresult['reason'];
				}
				
				return $valid;
			}
			curl_close($ch);
			update_option("wpexpro__api_failures",0);
			
			if($jresult['status'] == "valid") {
				$new_valid = true;
				update_option("wpexpro__num_sites",$jresult['num_sites']);
				update_option("wpexpro__num_exps",$jresult['num_exps']);
				update_option("wpexpro__num_tests",$jresult['num_tests']);
			} else {
				$new_valid = false;
			}

			if($valid != $new_valid) {
				update_option("wpexpro__valid_license",$new_valid);
				$valid = $new_valid;
			}
			update_option("wpexpro__license_check",$this->now);
		
			if($new_valid === FALSE) {
				if(isset($jresult['reason'])) return $jresult['reason'];
			}
		}
		
		return $valid;
	}

	function translateLicenseFail($reason) {
		switch($reason) {
			case 'license not specified':
				return 'License key was not specified.';
			case 'site_url not specified':
				return 'Site URL was not specified.';
			case 'not found':
				return 'License key is not valid.';
			case 'site limit reached':
				return 'Max number of installations reached.';
		}
		return "Unknown failure: $reason";
	}

	// Gets the current user's wordpress role
	function get_user_role() {
		global $current_user, $wp_roles;

		if($this->role) return $this->role;

		if( $current_user->id )  {
			foreach($wp_roles->role_names as $role => $Role) {
				if (array_key_exists($role, $current_user->caps))
					break;
			}
			$this->role = $role;
			return $this->role;
		}
		return NULL;
	}

	// Whether the current users' role exceeds the
	// neccessary role to use the plugin
	function can_use_plugin() {
		$roles = array(
			"administrator"=>0,
			"editor"=>1,
			"author"=>2,
			"contributor"=>3,
			"subscriber"=>4
		);
		$min_role = get_option("wpexpro__min_role");
		
		return $roles[$this->get_user_role()] <= $roles[$min_role];
	}

	// Whether the current user can edit the 
	// given experiment
	function can_edit_experiment($exid) {
		$exp = $this->getExperiment($exid);
		if(!$exp) return false;

		return $this->can_edit_post($exp['post_id']);
	}
	// Add the ajaxurl variable to all of the pages so we 
	// can use it on the front end
	function add_ajaxurl() {
		echo '<script type="text/javascript">';
		echo 'var ajaxurl = "'.admin_url('admin-ajax.php').'";';
		echo '</script>';
	}

	function is_bot() {
		global $_ROBOT_USER_AGENTS;
		
		if(isset($_SESSION['wpexpro_is_bot'])) {
			return $_SESSION['wpexpro_is_bot'];
		}

		$ua = $_SERVER['HTTP_USER_AGENT'];
		foreach($_ROBOT_USER_AGENTS as $agent) {
			if(preg_match("/".$agent."/i", $ua)) {
				$_SESSION['wpexpro_is_bot'] = TRUE;
				return TRUE;
			}
		}

		$_SESSION['wpexpro_is_bot'] = FALSE;
		return FALSE;
	}

	// Handles click goals and finish polls
	function record_trigger_check() {
		if(get_option("wpexpro__exclude_users") && is_user_logged_in()) return;
		if(get_option("wpexpro__exclude_bots", TRUE) && $this->is_bot()) return;
		
		if(isset($_REQUEST['wpexpro-record'])) {
			global $wpdb;
			if(count($_REQUEST['wpexpro-tests'])) {
				$sql = "UPDATE ".$this->test_tbl." SET conversions=conversions+1 WHERE id IN (".implode(",", $_REQUEST['wpexpro-tests']).");";
				$wpdb->query($sql);
			}
			if(count($_REQUEST['wpexpro-stats'])) {
				$sql = "UPDATE ".$this->stat_tbl." SET converted=1 WHERE id IN (".implode(",", $_REQUEST['wpexpro-stats']).");";
				$wpdb->query($sql);
			}
			exit();
		}
		if(isset($_REQUEST["wpexpro-finish"])){
			global $wpdb;
			$now = current_time('timestamp', 1);
			$sql = "UPDATE ".$this->stat_tbl." SET leave_ts=$now WHERE id IN (".implode(",", $_REQUEST['wpexpro-stats']).");";
			$wpdb->query($sql);
			exit();
		}
	}
	
	// Handles pageview goals
	function check_pageview_goals() {
		if(get_option("wpexpro__exclude_users") && is_user_logged_in()) return;
		if(get_option("wpexpro__exclude_bots", TRUE) && $this->is_bot()) return;

		// what is the "post_id" of the current page
		if(is_home() || is_front_page()) {
			$id = "root";
		}elseif(is_single() || is_page()) {
			global $post;
			$id = $post->ID;
		}
		if(isset($_SESSION['wpexpro-pv-goals'][$id])) {
			//found some!
			$stat_ids = $_SESSION['wpexpro-pv-goals'][$id];
			unset($_SESSION['wpexpro-pv-goals'][$id]);
			
			//Because of the way we set up $stat_ids, we should have an
			// associative array for (exp_id => stat_id) pairs which means we
			// can mark a conversion for every stat id (and associated test) without
			// concern that we will be over converting.
			if(!empty($stat_ids)) {
				global $wpdb;
				//first update the tests table
				$sql = "UPDATE ".$this->test_tbl." SET conversions=conversions+1 WHERE id IN (SELECT DISTINCT test_id FROM ".$this->stat_tbl." WHERE id IN (".implode(",", array_values($stat_ids))."));";
				$wpdb->query($sql);
				//then update the stat table
				$sql = "UPDATE ".$this->stat_tbl." SET converted=1 WHERE id IN (".implode(",", array_values($stat_ids)).");";
				$wpdb->query($sql);
			}
		}
	}
	
	// Get all the active experiments for the current page
	// that is being viewed
	function get_active_experiments() {
		global $post;

		// what is the "post_id" of the current page
		if(is_home() || is_front_page()) {
			$id = "root";
		}elseif(is_single() || is_page()) {
			$id = $post->ID;
		} else {
			return array();
		}
		if(isset($_REQUEST['wpexpro-exp-test-id'])) {
			$this->exps[$id] = $this->getExperiments(array("test_id"=>$_REQUEST['wpexpro-exp-test-id'],"post_id"=>$id));
		} else if(!isset($this->exps[$id])) {
			$this->exps[$id] = $this->getExperiments(array("post_id"=>$id,"status"=>self::STATUS_RUNNING));
		}

		$exps = $this->exps[$id];
		return $exps;
	}

	// If an experiment needs to be done, start output buffering
	function setup_output_buffering() {

		$exps = $this->get_active_experiments();
		if(count($exps) > 0) {
			//We need to start output buffering because we are going
			//to do some changes.
			ob_start(array($this,'finish_output_buffering'));
		}
	}

	// Output buffering callback.
	function finish_output_buffering($content) {

		$exps = $this->get_active_experiments();
		if(count($exps) > 0) {
			return $this->do_experiments($content);
		} else {
			return $content;
		}
	}

	// Get the best test from the experiment to show
	// the user. This is either the one they are bucketed into
	// or use our beta distribution algorithm
	function get_best_test($exp) {
		$test = null;
		//check for a bucket
		if($this->use_bucket && isset($_SESSION['wpexpro-exp-'.$exp['id']])) {
			$session_test_id = $_SESSION['wpexpro-exp-'.$exp['id']];
			// look for the test that is in the session
			foreach($exp['tests'] as $t) {
				if($t['id'] == $session_test_id) {
					return $t;
				}
			}
			// for some reason we didn't find the bucketed test
		}

		//Use a beta distribution random number to determine which 
		//test to show. Based on:
		// http://www.quora.com/In-A-B-Testing-how-many-conversions-do-you-need-per-variation-for-the-results-to-be-significant
		require_once dirname(__FILE__).'/libs/PDL/BetaDistribution.php';
		mt_srand();
		$max = 0;
		foreach($exp['tests'] as $t) {
			// Seed with a 1/2 = 50% probability
			$i = $t['impressions'] <= 1 ? 2 : $t['impressions'];
			$c = $t['conversions'] <= 1 ? 1 : $t['conversions'];

			if($i-$c == 0) $i++;

			$bd= new BetaDistribution($c,$i-$c);
			$r = $bd->_getRNG();
			if($r > $max) {
				$test = $t;
				$max = $r;
			}
		}
		
		// bucket this user into this test
		$_SESSION['wpexpro-exp-'.$exp['id']] = $test['id'];
		return $test;
	}

	// Carry out the experiment on our page by manipulating
	// the content as we see fit.
	function do_experiments($content) {
		global $post, $wpdb;
		$exps = $this->get_active_experiments();
		// this shouldn't ever be the case, but just in case
		if(count($exps) == 0) {
			return $content;
		}
		include 'libs/querypath/qp.php';
		// something went wrong;
		if(!function_exists('qp')) {
			error_log("Failed to load QueryPath!");
			return $content;
		}
		$qp = htmlqp($content);

		$goals = array();
		$tests_ran = array();
		$stat_ids = array();
		$now = current_time('timestamp', 1);
		foreach($exps as $exp) {

			if($_REQUEST['wpexpro-exp-test-id']) {
				$found = FALSE;
				foreach($exp['tests'] as $test) {
					if($test['id'] == $_REQUEST['wpexpro-exp-test-id']) {
						$found = TRUE;
						break;
					}
				}
				// if we didn't find the specified test id, continue. We don't
				// want to apply any tests but the one asked for.
				if(!$found) continue;
			} else {
				$test = $this->get_best_test($exp);
			}
		
			if($exp['goal_type'] == self::GOAL_CLICK) {
				if(!isset($goals[$exp['goal']])) {
					$goals[$exp['goal']] = array();

				}
				$goals[$exp['goal']][] = $test['id'];	
			}
			
			$tests_ran[] = $test['id'];

			if($test['data'] && count($test['data']) != 0) {
				foreach($test['data'] as $step) {
					switch($step['action']) {
						case 'replace':
							try{
								$e = $this->get_element($qp,$step['element']);
								if($e) {
									$e->replaceWith(htmlqp($step['content']));
								}
							}catch(Exception $e) {
								error_log("Error while applying step: ". $e);
								error_log(serialize($step));
							}
						break;
						case 'style':
							try{
								$e = $this->get_element($qp,$step['element']);
								if($e) {
									$e->css($step['styles']);
								}
							}catch(Exception $e) {
								error_log("Error while applying step: ". $e);
								error_log(serialize($step));
							}
						break;
						case 'classes':
							try{
								$e = $this->get_element($qp,$step['element']);
								if($e) {
									$e->attr("class","");
									$e->addClass(implode($step['classes']," "));
								}
							}catch(Exception $e) {
								error_log("Error while applying step: ". $e);
								error_log(serialize($step));
							}
						break;
						case 'delete':
							try{
								$e = $this->get_element($qp,$step['element']);
								if($e) {
									$e->remove();
								}
							}catch(Exception $e) {
								error_log("Error while applying step: ". $e);
								error_log(serialize($step));
							}
						break;
						case 'script':
							if(!isset($_REQUEST['noscript'])) {
								try{
									$qp->find("head")->append("<script type='text/javascript'>jQuery(document).ready(function() {".$step['script']."});</script>");
								}catch(Exception $e) {
									error_log("Error while applying step: ". $e);
									error_log(serialize($step));
								}	
							}
						break;
					}
				}
			}

			$data = array(
				"test_id" => $test['id'],
				"ts" => $now,
				"leave_ts" => $now,
				"ip" => $_SERVER['REMOTE_ADDR'],
				"session_id" => session_id()
			);
			// Don't count impressions for the dashboard's iframe
			if(!isset($_REQUEST['inadmin'])) {
				// Don't count impressions for logged in users if configured
				if(!(get_option("wpexpro__exclude_users") && is_user_logged_in()) &&
					!(get_option("wpexpro__exclude_bots", TRUE) && $this->is_bot())) {
					$wpdb->insert($this->stat_tbl, $data);
					$last_stat_id = $wpdb->insert_id;
					$stat_ids[$test['id']] = $last_stat_id;
			
					//If this experiment has a pageview goal type, we need
					// to store these stat_ids in our session so we can check
					// them later 
					if($exp['goal_type'] == self::GOAL_PAGEVIEW) {
						if(!isset($_SESSION['wpexpro-pv-goals'])) {
							$_SESSION['wpexpro-pv-goals'] = array();
						}
						if(!isset($_SESSION['wpexpro-pv-goals'][$exp['goal']])) {
							$_SESSION['wpexpro-pv-goals'][$exp['goal']] = array();
						}
						$_SESSION['wpexpro-pv-goals'][$exp['goal']][$exp['id']] = $last_stat_id;
					}
				}
			}
		}

		$teststr = implode(",", $tests_ran);
		$dataStr = json_encode(array("tests"=>$goals,"url"=>site_url(), "stats"=>$stat_ids));
		$qp->find("head")->append("<script type='text/javascript'>var _wpexproData = $dataStr;</script>");
		// Don't count impressions for the dashboard's iframe
		if(!isset($_REQUEST['inadmin'])) {
			// Don't count impressions for logged in users if configured
			if(!(get_option("wpexpro__exclude_users") && is_user_logged_in()) &&
			   !(get_option("wpexpro__exclude_bots", TRUE) && $this->is_bot())) {
				$sql = "UPDATE ".$this->test_tbl." SET impressions=impressions+1 WHERE id IN ($teststr);";
				$wpdb->query($sql);
			}
		}
		return $qp->html();
	}

	function print_r($arr,$indent = "") {
		$ret = "";
		foreach($arr as $k=>$v) {
			if(is_array($v)) {
				$ret .= $indent . "[$k] => \n" . $this->print_r($v,$indent."  ") . "\n";
			} else {
				$ret .= $indent . "[$k] => $v\n";
			}
		}
		return $ret;
	}

	// Get a DOM element from the selector, taking into account
	// that the selector may be one of our funny reverse parent selectors
	private function get_element($qp,$selector) {
		$pieces = explode("<",$selector);
		if(count($pieces) > 1) {
			$elm = $qp->find(trim($pieces[0]));
			for($i = 0; $i < count($pieces) - 1; $i++) {
				$elm = $elm->parent();
				if(!$elm) {
					break;	
				}
			}
			return $elm;
		} else {
			$elm = $qp->find(trim($selector));
			return $elm;
		}
	}

	// Count the number of experiments that have a given status
	function countExpsByStatus($stat= null) {
		global $wpdb;

		$filter = array();
		if($stat !== NULL && in_array($stat,array_keys($this->STATUS_STR))) { 
			$filter['status'] = $stat;
		}
		$exps = $this->getExperiments($filter,false,false);
		$counter = 0;
		foreach($exps as $idx=>$exp) {
			if($this->can_edit_post($exp['post_id'])) {
				$counter++;
			}
		}
		return $counter;
	}

	// Count all of the experiments
	function countExps() {
		global $wpdb;
		return $this->countExpsByStatus(null);
	}
	
	// Get all of the posts/pages/root that the user can edit
	function getPosts() {
		$pages = get_pages();
		$posts = get_posts(array("numberposts"=>"-1","orderby"=>"post_title"));
		//remove pages this user can't edit
		foreach($pages as $idx=>$page) {
			if(!current_user_can('edit_post',$page->ID)) {
				unset($pages[$idx]);
			}
		}
		$posts = get_posts(array("numberposts"=>"-1","orderby"=>"post_title"));
		//remove posts this user can't edit
		foreach($posts as $idx=>$post) {
			if(!current_user_can('edit_post',$post->ID)) {
				unset($posts[$idx]);
			}
		}
		$root_id = get_option('page_on_front');
		$inc_root = false;
		if($root_id) {
			$inc_root = current_user_can("edit_post",$root_id);
		} else {
			$inc_root = current_user_can("manage_options");
		}
		return array($posts,$pages,$inc_root);
	}

	// Can the current user edit this "post" (which may be 
	// a post, page, or "root")
	function can_edit_post($id) {
		// most typically when the root page isn't a real page
		if(!$id) return $this->get_user_role() == "administrator";
		
		if($id == "root") {
			return $this->can_edit_post(get_option('page_on_front'));
		} else {
			return current_user_can("edit_post",$id);
		}
	}
	// Get an experiment
	function getExperiment($id, $load_tests = true, $load_test_stats = false) {
		$ret = $this->getExperiments(array("id"=>$id),$load_tests,$load_test_stats);
		if(is_array($ret) && count($ret) > 0) {
			return array_pop($ret);
		} else {
			return NULL;
		}
	}

	// Get lots of experiments based on filters
	function getExperiments($filter=NULL, $load_tests = true, $load_test_stats = false) {
		global $wpdb;

		$sql = "SELECT * FROM " . $this->exp_tbl;
		if($filter) {
			$where = array();
			$data = array();
			$cols = array("id"=>"%d","status"=>"%d","post_id"=>"%d");
			foreach($cols as $col=>$format) {
				if(isset($filter[$col])) {
					$where[] = "$col = $format";
					$data[] = $filter[$col];
				}
			}
			
			$sql .= " WHERE " . join($where,' AND ');
			$sql = $wpdb->prepare($sql, $data);
		}
		$rows = $wpdb->get_results($sql,ARRAY_A);

		if(is_admin()) {
			// Control access
			foreach($rows as $idx=>$row) {
				if($row['post_id'] == "root") {
					$id = get_option('page_on_front');
					if($id) {
						if(!current_user_can("edit_post",$id)) {
							unset($rows[$idx]);
						}
					} else {
						// if the homepage is not set to a page, then don't show it.
						if(!current_user_can("manage_options")) {
							unset($rows[$idx]);
						}
					}
				} else {
					if(!current_user_can("edit_post",$row['post_id'])) {
						unset($rows[$idx]);
					}
				}
			}
		}

		if($load_tests) {	
			foreach($rows as $idx=>&$row) {
				$sql = "SELECT * FROM " .$this->test_tbl;
				$where = array("exp_id = %d");
				$data = array($row['id']);
				if(isset($filter['test_id'])) {
					$where[] = "id = %d";
					$data[] = $filter['test_id'];
				}
				$sql .= " WHERE " . join($where, ' AND ') . " ORDER BY id ASC";
				$sql = $wpdb->prepare($sql, $data);
				$row['tests'] = $wpdb->get_results($sql,ARRAY_A);
				if(is_array($row['tests'])) {
					foreach($row['tests'] as &$test) {
						$test['result'] = "unknown";
						if($test['data']) {
							$test['data'] = unserialize($test['data']);
						}
					}
				}
			}
		}

		if($load_test_stats) {
			require_once dirname(__FILE__).'/libs/PDL/BetaDistribution.php';
			
			foreach($rows as &$row) {
				if(is_array($row['tests'])) {
					foreach($row['tests'] as &$test){
						$c = $test['conversions'] > 1 ? $test['conversions'] : 1;
						$i = $test['impressions'] > 1 ? $test['impressions'] : 2;

						if($i-$c == 0) $i++;

						$test['bd'] = new BetaDistribution($c,$i-$c);
					}
				}
			}
			

			foreach($rows as &$row) {
				$this->statTests = $row['tests'];
				if(is_array($row['tests'])) {
					foreach($row['tests'] as $idx=>&$test) {
						$this->statChecking = $idx;
						$test['probability'] = round($this->simpsonsrule() * 100);
					}
				}
			}
		}

		return stripslashes_deep($rows);
	}

	// Out simpson rule integral approximation f(x)
	function simpsonf($x){
		$prod = 1;
		foreach($this->statTests as $id=>$test) {
			if($id == $this->statChecking) {
				$prod *= $test['bd']->_getPDF($x);
			} else {
				$prod *= $test['bd']->_getCDF($x);
			}
		}
		// returns f(x) for integral approximation with composite Simpson's rule
		 return $prod;
	}

	// Implementation of Simpsons Rule for integral approximations
	// From: http://www.php.net/manual/en/ref.math.php#61377
	function simpsonsrule(){
		$a = 0; $b = 1; $n = 1000;
		// approximates integral_a_b f(x) dx with composite Simpson's rule with $n intervals
		// $n has to be an even number
		// f(x) is defined in "function simpsonf($x)"
		 if($n%2==0){
				$h=($b-$a)/$n;
				$S=$this->simpsonf($a)+$this->simpsonf($b);
				$i=1;
				while($i <= ($n-1)){
					 $xi=$a+$h*$i;
					 if($i%2==0){
							$S=$S+2*$this->simpsonf($xi);
					 }
					 else{
							$S=$S+4*$this->simpsonf($xi);
					 }
					 $i++;
				}
				return($h/3*$S);
				}
		 else{
				return('$n has to be an even number');
		 }
	}

	// Use a session to flash a message to the user
	function flash($msg,$type='') {
		if(!isset($_SESSION['wpexpro-flash'])) {
			$_SESSION['wpexpro-flash'] = array();
		}
		$_SESSION['wpexpro-flash'][] = array('msg'=>$msg,'type'=>$type);
	}
	
	// Display any flash messages that we have
	function display_flash(){
		if(isset($_SESSION['wpexpro-flash'])) {
			$flash = '';
			foreach($_SESSION['wpexpro-flash'] as $f) {
				 $flash .= '<p class="alert '.$f['type'].'">'.$f['msg'].'<a class="close-flash" onclick="wpexproCloseFlash(this);">&#215;</a></p>';
			}
			echo $flash;
			unset($_SESSION['wpexpro-flash']);
		}
	}
	
	// Count the number of tests in an experiment
	function countTests($id) {
		global $wpdb;
		if(!preg_match("/^[0-9]+$/",$id)) return 0;

		$sql = "SELECT COUNT(*) FROM " .$this->test_tbl." WHERE exp_id=".$id;
		return $wpdb->get_var($sql) - 1; //exclude the orginal
	}

	// Get the URL of a post
	function getPostUrl($id) {
		$p = get_post($id);
		return $p->post_title;
	}

	// Get the title of a post
	function getPostTitle($id) {
		if($id == "root") {
			return "Home Page";
		} else {
			$p = get_post($id);
			if($p) {
				return $p->post_title;
			}
		}
		return "Unknown Page (".$id.")";
	}

	// Get the author user name of a post
	function getUserName($id) {
		$user = get_userdata($id);
		return $user->display_name;
	}

	// Translate a status integer into a string
	function translateStatus($status) {
		return $this->STATUS_STR[$status];
	}

	// Redirect to a page with an optional flash message
	function redirect_to($page,$flash=FALSE) {
		if($flash) {
			$this->flash($flash);
		}
		$redirect_to = $page;
		include 'wpexpro-redirect.php';
		exit();
	}

	// Register all of our menu items
	function register_menu() {

		if(!$this->can_use_plugin()) return;

		$menu_slug =  basename(dirname(__FILE__)).'/wpexpro-admin-menu.php';
		add_menu_page( 'wp-experiments-pro', 'Experiments', 'edit_posts', $menu_slug, '', 'dashicons-chart-bar', 85 );
		add_submenu_page(NULL, 'Edit Experiment', 'Edit Experiment', 'edit_posts', "wpexpro-edit-experiment", array($this,"edit_experiment_page") );
		add_submenu_page($menu_slug, 'New Experiment', 'New Experiment', 'edit_posts', "wpexpro-new-experiment", array($this,"new_experiment_page") );

		// Only admins can see the settings
		if($this->get_user_role() == "administrator")
			add_submenu_page($menu_slug, 'Settings', 'Settings', 'edit_posts', "wpexpro-settings", array($this,"general_settings") );
	}

	// Register a menu to set out license
	function register_license_menu() {
		$menu_slug =  basename(dirname(__FILE__)).'/wpexpro-admin-menu.php';
		add_menu_page( 'wp-experiments-pro', 'Experiments', 'edit_posts', $menu_slug, '', plugins_url( basename(dirname(__FILE__)).'/img/beaker-gray.png' ), 85 );
	}

	// The general settings page
	function general_settings() {
		if($this->get_user_role() != "administrator") return;

		if(isset($_REQUEST['save'])) {
			update_option("wpexpro__exclude_users",$_REQUEST['exclude_users']);
			update_option("wpexpro__exclude_bots",$_REQUEST['exclude_bots']);
			update_option("wpexpro__min_role",$_REQUEST['role']);
			$old_license = get_option("wpexpro__license");
			if($old_license !== $_REQUEST['license'] && substr($_REQUEST['license'],0,4) != "XXXX"){
				update_option("wpexpro__license",$_REQUEST['license']);
				update_option("wpexpro__license_check",current_time('timestamp', 1)-12*60*60-60);
				if(!$this->do_license_check()) {
					$this->redirect_to($this->pages['index'], "License invalidated.");
				} else {
					//The license still looks good
				}
			}
			
			update_option("wpexpro__use_bucket",isset($_REQUEST['bucket']) ? 1 : 0);
		}
		
		$exclude_users = get_option("wpexpro__exclude_users");
		$exclude_bots = get_option("wpexpro__exclude_bots", TRUE);
		$role = get_option("wpexpro__min_role");
		if($role === FALSE) $role = "author";
		$license = get_option("wpexpro__license");
		$num_sites = get_option("wpexpro__num_sites");
		$num_exps = get_option("wpexpro__num_exps");
		$num_tests = get_option("wpexpro__num_tests");
		$bucket = get_option("wpexpro__use_bucket");
		if($bucket === FALSE) $bucket = 1;
		include 'wpexpro-general-settings.php';
	}

	// The edit experiment page
	function edit_experiment_page() {
		global $wpdb;

		$valid_actions = array("trash","save","reset","start","stop","pause");
		if(!isset($_REQUEST['exid']) || preg_match("/[^\d]/",$exid)) {
			$this->redirect_to($this->pages['index']);
		}
		$exid = $_REQUEST['exid'];
		
		if(!$this->can_edit_experiment($exid)) {
			$this->redirect_to($this->pages['index'], "You don't have permission to edit this experiment");
		}

		if(isset($_REQUEST['action']) && in_array($_REQUEST['action'], $valid_actions)) {

			switch($_REQUEST['action']) {
				case 'save':
					$data = json_decode(stripslashes($_REQUEST['data']),true);
					
					$genattr = array("title","description","goal","goal_type");
					$new_data = array();
					foreach($genattr as $att) {
						$new_data[$att] = $data[$att];
					}
					$wpdb->update($this->exp_tbl, $new_data, array("id"=>$exid));

					foreach($data['deleted_tests'] as $test) {
						//make sure this test belongs to this experiment
						if($wpdb->get_row( $wpdb->prepare("SELECT * FROM $this->test_tbl WHERE id=%d AND exp_id=%d",$test,$exid))) {
							$wpdb->delete($this->test_tbl,array("id"=>$test,"exp_id"=>$exid));
							$wpdb->delete($this->stat_tbl,array("test_id"=>$test));
						}
					}

					$test_limit = get_option("wpexpro__num_tests");
					if($test_limit == -1) $test_limit = 200;

					$test_count = 0;
					foreach($data['tests'] as $test) {
						$test_count += 1;
						if($test_count > $test_limit) break;

						$new_data = array(
							"name"=>$test['name'],
							"description"=>$test["description"],
							"data"=>serialize($test['steps'])
						);
						if(strlen(trim($test['id']))>0) {
							$wpdb->update($this->test_tbl,$new_data,array("id"=>$test['id']));
						} else {
							$new_data['exp_id'] = $exid;
							$wpdb->insert($this->test_tbl,$new_data);
						}
					}
					
					

					$this->redirect_to($this->pages['edit'].$exid,"Experiment saved.");
					break;
				case 'reset':
					$exp = $this->getExperiment($exid,true);
					foreach($exp['tests'] as $test) {
						$test_ids[] = $test['id'];
					}
				
					if(count($test_ids) > 0) {
						$sql = "DELETE FROM ".$this->stat_tbl." WHERE test_id IN (".implode(",", $test_ids).");";
						$wpdb->query($sql);
						$sql = "UPDATE ".$this->test_tbl." SET impressions=0,conversions=0 WHERE id IN (".implode(",", $test_ids).");";
						$wpdb->query($sql);
						$sql = "UPDATE ".$this->exp_tbl." SET started_ts=".current_time("timestamp",1)." WHERE id=".$exp['id'];
						$wpdb->query($sql);
					}
					$this->redirect_to($this->pages['edit'].$exid,"Experiment results have been reset.");
					break;
				case 'start':
					$exp_limit = get_option("wpexpro__num_exps");
					$sql = "SELECT COUNT(*) FROM ".$this->exp_tbl." WHERE status=".self::STATUS_RUNNING;
					$count = $wpdb->get_var($sql);
					if($exp_limit != -1 && $count >= $exp_limit) {
						$this->redirect_to($this->pages['edit'].$exid,"Cannot start experiment. Simultaneous experiment limit reached.");
					}
				case 'stop':
				case 'pause':
					$ss = array("start"=>self::STATUS_RUNNING, "stop"=>self::STATUS_STOPPED, "pause"=>self::STATUS_PAUSED);
					$data = array("status"=>$ss[$_REQUEST['action']]);
					if($_REQUEST['action'] == "start") {
						$exp = $this->getExperiment($exid,false,false);
						if(!$exp['started_ts'])
							$data['started_ts'] = current_time("timestamp", 1);
					}
					$wpdb->update($this->exp_tbl,$data,array("id"=>$exid));
					$this->redirect_to($this->pages['edit'].$exid,"Experiment is ".strtolower($this->STATUS_STR[$ss[$_REQUEST['action']]]).".");
					break;
				case 'trash':
					$exp = $this->getExperiment($exid,true);
					foreach($exp['tests'] as $test) {
						$test_ids[] = $test['id'];
					}
					
					if(count($test_ids) > 0) {
						$sql = "DELETE FROM ".$this->stat_tbl." WHERE test_id IN (".implode(",", $test_ids).");";
						$wpdb->query($sql);
						$sql = "DELETE FROM ".$this->test_tbl." WHERE id IN (".implode(",", $test_ids).");";
						$wpdb->query($sql);
					}
					$sql = "DELETE FROM ".$this->exp_tbl." WHERE id =".$exid;
					$wpdb->query($sql);
					$this->redirect_to($this->pages['index'],"Experiment deleted.");
				break;
			}
		} else {
			add_filter('tiny_mce_before_init', array($this,'add_tiny_mce_callback'));
			$exp = $this->getExperiment($exid,true,true);
			$exp["edit_url"] = $this->pages['edit'].$exp['id'];
			
			$exp_limit = get_option("wpexpro__num_exps");
			if($exp_limit == -1) {
				$show_start_button = true;
			} else {
				$sql = "SELECT COUNT(*) FROM ".$this->exp_tbl." WHERE status=".self::STATUS_RUNNING;
				$running_count = $wpdb->get_var($sql);	
				$show_start_button = $running_count < $exp_limit;
			}
			

			include 'wpexpro-edit-experiment.php';
		}
	}

	// This grabs Wordpress's TinyMCE settings and puts them
	// into a javascript object so we can use them dynamically
	// and make our editor's look like native wordpress ones
	function add_tiny_mce_callback($in) {
		echo "<script type='text/javascript'>";
		$formats = $in['formats'];
		unset($in['formats']);
		echo "wpexproTinyMCESettings = ".json_encode($in).";\n";
		echo "wpexproTinyMCESettings.formats = " . $formats . ";\n";
		echo "</script>";
		return $in;
	}

	// The new experiment page
	function new_experiment_page() {

		if(isset($_POST['submit'])) {
			global $wpdb;
			global $current_user;
					get_currentuserinfo();

			$exp = $_POST['experiment'];
			$exp['user_id'] = $current_user->ID;
			$exp['status'] = self::STATUS_NEW;

			$errors = $this->validate($exp);

			if(empty($errors)) {
				$data = array(
					'user_id' => $current_user->ID,
					'status' => self::STATUS_NEW,
					'post_id' => $_POST['post_id'],
					'title' => $_POST['title'],
					'description' => $_POST['description']
				);
				$wpdb->insert($this->exp_tbl, $exp, "%s");
				$test = array(
					"exp_id"=>$wpdb->insert_id,
					"name"=>"Original",
					"description"=>"",
				);
				$wpdb->insert($this->test_tbl, $test);

				$this->redirect_to($this->pages['index'],"Experiment created.");
				// die();
			} else {
				include 'wpexpro-new-experiment.php';
			}
		} else {
			$exp = array('title'=>'','description'=>'','post_id'=>'');
			include 'wpexpro-new-experiment.php';
		}
	}

	// An ajax method to getStatChartData()
	function ajax_get_stats() {
		if($_REQUEST['from']) {
			$from = strtotime($_REQUEST['from']);
			if($from === FALSE) {
				die(json_encode(array("status"=>"bad ts","which"=>"from")));
			}		
		} else {
			$from = NULL;
		}

		if($_REQUEST['to']) {
			$to = strtotime($_REQUEST['to']);
			if($to === FALSE) {
				die(json_encode(array("status"=>"bad ts","which"=>"to")));
			}
		} else {
			$to = NULL;
		}
		
		
		if($to !== NULL && $from !== NULL && $to < $from) {
			$t = $to;
			$to = $from;
			$from = $t;
		}

		$exp = $this->getExperiment($_REQUEST['expid'],true,true);
		unset($exp['bd']);
		$ret['exp'] = $exp;
		$ret['data'] = $this->getStatChartData($exp,$from,$to);
		echo json_encode($ret);
		die();
	}

	// Get conversion statistics chart data
	function getStatChartData($exp, $min_ts = null, $max_ts = null) {
		global $wpdb;
		$tzoff = get_option('gmt_offset') * 3600;

		$results = array(
			"type"=>"line",
			"template"=>"line_basic_4",
			"tooltips"=>array(),
			"labels"=>array(),
			"values"=>array()
		);

		$test_ids = array();
		foreach($exp['tests'] as $test) {
			$test_ids[] = $test['id'];
		}
		
		$MINUTE = 60;
		$HOUR = 60*$MINUTE;
		$DAY = 24*$HOUR;
		$WEEK = 7*$DAY;

		if(count($test_ids) == 0) return array();
		$max_ts = $max_ts === null ? current_time('timestamp', 1) : $max_ts;
		$min_ts = $min_ts === null ? $exp['started_ts'] : $min_ts;
		
		if(!$min_ts && !$exp['started_ts']) return array(); //exp hasn't started yet

		$t_diff = $max_ts - $min_ts;
		
		if($t_diff == 0) {
			// Just the one dot
			$delta = 1;
		} else if($t_diff <= 10*$MINUTE) {
			$delta = 1*$MINUTE;
		} else if($t_diff <= 30*$MINUTE) {
			$delta = 5*$MINUTE;
		} else if($t_diff <= 1*$HOUR) {
			// Use 10m blocks for the current hour
			$delta = 10*$MINUTE;
		} else if($t_diff <= 2*$HOUR) {
			// Use 10m blocks for the current hour
			$delta = 20*$MINUTE;
		} else if($t_diff <= 4*$HOUR) {
			// Use 10m blocks for the current hour
			$delta = 30*$MINUTE;
		} else if($t_diff < 12*$HOUR) {
			// Use 1h blocks
			$delta = 1*$HOUR;
		} else if($t_diff < 1*$DAY) {
			// Use 2h blocks
			$delta = 2*$HOUR;
		} else if($t_diff < 4*$DAY) {
			// Use 8h blocks
			$delta = 8*$HOUR;
		} else if($t_diff < 3*$WEEK) {
			// Put into 1 day blocks
			$delta = 1*$DAY;
			// Since 1 day blocks is a special case because we need
			// to adjust the timezone so we can start at midnight. We
			// need to fudge everything by the timezone
			$start_ts = ($min_ts - $min_ts%$delta) - $tzoff;
			$adjust = $tzoff;
		} else {
			$delta = round(($max_ts - $min_ts) / 8);
			$delta = $delta - ($delta % $HOUR);
		}

		$adjust = $adjust === null ? 0 : $adjust;
		$start_ts = $start_ts === null ? $min_ts - $min_ts%$delta : $start_ts;

		$sql = "SELECT ts DIV $delta AS delta_ts,test_id, COUNT(ts) AS impressions, SUM(converted) AS conversions FROM ".
			$this->stat_tbl." WHERE ts >= $min_ts AND ts <= $max_ts AND test_id IN(".implode(",", $test_ids).") GROUP BY test_id,delta_ts ORDER BY delta_ts;";
		$dbresults = $wpdb->get_results($sql,ARRAY_A);
		$data = array();
		foreach($test_ids as $test_id) {
			$data[$test_id] = array();	
		}

		$df = get_option("date_format");
		$tf = get_option("time_format");
		$dtf = "$df $tf";
		$labels = array();
		for($i = $start_ts; $i < $max_ts; $i+=$delta) {
			$results['labels'][] = $this->dateRangeToStr($i+$tzoff, $delta,$min_ts - $min_ts%$delta, $max_ts,$tzoff);
			foreach($test_ids as $test_id) {
				$data[$test_id][$i] = array("test_id"=>$test_id,"impressions"=>0,"conversions"=>0,"start_ts"=>date($dtf,$i+$tzoff),"end_ts"=>date($dtf,$i+$tzoff+$delta));
			}
		}
		
		foreach($dbresults as $row) {
			if(isset($data[$row['test_id']][$row['delta_ts']*$delta-$adjust]) && is_array($data[$row['test_id']][$row['delta_ts']*$delta-$adjust])) {
				$data[$row['test_id']][$row['delta_ts']*$delta-$adjust] = array_merge($data[$row['test_id']][$row['delta_ts']*$delta-$adjust], $row);
			}
		}
		$counter = 1;
		$results['_series'] = array();
		foreach($data as $test_id=>$d) {
			$results['_series'][$counter] = array_values($d);
			$results['values']["serie".$counter] = array();
			foreach($d as $k=>$v) {
				$results['values']["serie".$counter][] = $v['conversions'];
			}
			$counter++;
		}
		return $results;
	}

	// Translate a date range in the shortest friendly string we can think of
	function dateRangeToStr($segment,$delta,$start,$end,$tzoff,$inc_next=true) {
		// if the delta is 1 day, and we are starting at midnight, don't show the next date
		if(($delta == 24*60*60) && ($start % (24*60*6) == 0)) {
			$inc_next = false;
		}
		$next = $inc_next ? (" - " . $this->dateRangeToStr($segment + $delta, $delta, $start,$end,$tzoff,false)) : "";
		if($delta < 24*60*60) {
			$sday = $start - (($start+$tzoff) % (24*60*60));
			$eday = $end - (($end+$tzoff) % (24*60*60));
			if($sday == $eday) {
				//the entire range is less than a day and in the same day, only do the time
				return date(get_option('time_format'),$segment).$next;
			} else {
				// this makes it a little harder because we want to include the days were they change
				if($segment == $start) {
					return date(get_option('date_format')." ".get_option('time_format'),$segment).$next;
				} else {
					$segday = $segment - (($segment+$tzoff) % (24*60*60)) ;
					$last = $segment - $delta;
					$lastday = $last - (($last+$tzoff) % (24*60*60));
					if($segday != $lastday) {
						return date(get_option('date_format')." ".get_option('time_format'),$segment).$next;
					} else {
						return date(get_option('time_format'),$segment).$next;
					}
				}
			}
		} else {
			if(($delta+$tzoff) % 24*60*60 == 0) {
				return date(get_option('date_format'),$segment).$next;
			} else {
				return date(get_option('date_format')." ".get_option('time_format'),$segment).$next;
			}
		}
	}

	// Enqueue all of our scripts and styles for the admin dashboard
	function admin_enqueue() {
		wp_enqueue_style('admin-wpexprocss', plugins_url('css/wpexpro-admin.css',__FILE__));

		wp_enqueue_script('wpexprojs', plugins_url('js/wpexpro.js',__FILE__), array('jquery','editor'), "1.24");
		wp_enqueue_script('wpexproadminjs', plugins_url('js/wpexpro_admin.js',__FILE__), array('jquery','editor'), "1.24");
		wp_enqueue_script('wpexprohelpersjs', plugins_url('js/wpexpro_helpers.js',__FILE__), array('jquery','editor'), "1.24");
		wp_enqueue_script('wpexprotestcasejs', plugins_url('js/wpexpro_test_cases.js',__FILE__), array('jquery','editor'), "1.24");
		wp_enqueue_script('wpexprooriginaljs', plugins_url('js/wpexpro_original.js',__FILE__), array('jquery','editor'), "1.24");
		
		wp_enqueue_script('jquery.sparkline.min.js', plugins_url('js/jquery.sparkline.min.js',__FILE__), array('jquery'), "1.24");
		wp_enqueue_script('jquery.color.min.js', plugins_url('js/jquery.color.min.js',__FILE__), array('jquery'), "2.1.2");
		
		wp_enqueue_script('uri.js', plugins_url('js/uri.js',__FILE__), array('jquery'), "1.24");

		wp_enqueue_script('jquery.qtip.min.js', plugins_url('js/jquery.qtip.min.js',__FILE__), array('jquery'), "1.24");
		wp_enqueue_style('jquery.qtip.min.css', plugins_url('css/jquery.qtip.min.css',__FILE__));

		wp_enqueue_script('jquery.reveal.js', plugins_url('js/jquery.reveal.js',__FILE__), array('jquery'), "1.24");
		wp_enqueue_style('reveal.css', plugins_url('css/reveal.css',__FILE__));

		wp_enqueue_script('codemirror.js', plugins_url('js/codemirror.js',__FILE__), array(), "1.24");
		wp_enqueue_style('codemirror.css', plugins_url('css/codemirror.css',__FILE__));

		wp_enqueue_script('raphael.js', plugins_url('js/raphael.js',__FILE__), array(), "1.5.2");
		wp_enqueue_script('elycharts.js', plugins_url('js/elycharts.js',__FILE__), array(), "2.1.4");
	}

	// Enqueue all of our scripts and styles for the main page
	function enqueue() {
		wp_enqueue_style('wpexprocss', plugins_url('css/wpexpro.css',__FILE__));
		wp_enqueue_script('wpexpro_helpers', plugins_url('js/wpexpro_helpers.js',__FILE__), array('jquery'), "1.10");
		wp_enqueue_script('wpexpro_enable_exp', plugins_url('js/wpexpro_enable_exp.js',__FILE__), array('jquery'), "1.10");
	}

	//Silly little function to check whether the
	//passed is status is the one we are displaying right now
	// and return "current" (which is the class name for the list)
	function currentSubSub($stat) {
		
		if(isset($_REQUEST['filter']) && isset($_REQUEST['filter']['status'])) {
			if($_REQUEST['filter']['status'] == $stat) {
				return "current";
			}
		} elseif ($stat === "") {
			return "current";
		}
		return "";
	}

	// Checks the validatey of an experiment
	//   @title cannot be blank
	//   @post_id cannot be blank
	function validate($exp) {
		$errors = array();
		if(!isset($exp['title']) || trim($exp['title']) == "") {
			$errors["title"] = "Please set a title for this experiment";
		}
		if(!isset($exp['post_id']) || trim($exp['post_id']) == "") {
			$errors["post_id"] = "Please set a page for this experiment";
		}

		return $errors;
	}
}

$wpexpro_obj = new WPExPro();
