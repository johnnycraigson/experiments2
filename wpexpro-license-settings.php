<div class='wrap'>
	<div id="icon-edit-pages" class="icon32 icon32-posts-page"><br></div>
	<h2>WP Experiments Settings</h2>
	<?php if(strlen($license) == 0): ?>
		<tr valign="top">
			<?php $started = get_option("wpexpro__installed"); 
				$current = current_time("timestamp");
				global $_FREE_TRIAL_LENGTH;
				$remaining = ($_FREE_TRIAL_LENGTH*24*60*60) - ($current - $started);
				if($remaining < 0):
			?>
				<p class="alert">Free Trial has ended. <a href='https://wpexperiments.com'>Upgrade Now!</a></p>
			<?php else: ?>
				<p class="alert">Free Trial ends in <?php echo ceil($remaining/(24*60*60)) . " days"; ?></p>
			<?php endif; ?>
		</tr>
	<?php endif; ?>
	<form method="post">
		<table class="form-table">
			<tbody>
				<tr valign="top">
					<th scope="row"><label for="blogname">License Key</label></th>
					<td>
						<input name="license" type="text" value="<?php echo wpexpro_key_hide($license); ?>" class="regular-text">
						<p class="description">Enter your license key to activate the plugin.</p>
						<?php if(isset($license_fail_reason)): ?>
							<p class="description error"><?php echo $license_fail_reason;?> If you need assistance, please contact me at <a href="mailto:jason@wpexperiments.com">jason@wpexperiments.com</a>.
						<?php endif; ?>
					</td>
				</tr>
				
				<tr valign="top">
					<th scope="row"><label for="blogname">Help or Support?</label></th>
					<td>
						Do you need help or support? Check out <a target="_blank" href="http://www.wpexperiments.com">http://www.wpexperiments.com</a> or email me at <a href="mailto:jason@wpexperiments.com">jason@wpexperiments.com</a>.
					</td>
				</tr>
				<tr>
					<th></th>
					<td><input type="submit" class="button-primary" name="save-license" value="Save Settings" /></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>