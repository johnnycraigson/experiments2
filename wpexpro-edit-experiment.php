<?php 
	list($posts,$pages,$inc_root) = $this->getPosts();
	$test_limit = get_option("wpexpro__num_tests");
?>
<script type='text/javascript'>
	wpexproData = <?php echo json_encode($exp); ?>;
	wpexproChartData = <?php echo json_encode($this->getStatChartData($exp)); ?>;
	wpexproTestLimit = <?php echo $test_limit ? $test_limit : -1; ?>;
</script>

<div class="wrap">
	<form id="wpexpro-form" method="post">
		<input type="hidden" value="save" name="action" />
		<input type="hidden" value="" name="data" />
	</form>
	<div id="icon-edit-pages" class="icon32 icon32-posts-page"><br></div>
	<h2 class="nav-tab-wrapper wpexpro-nav-tab-wrapper">
		<a href="#wpexpro-exp-general" class="wpexpro-tab nav-tab nav-tab-active">Edit Experiment</a>
		<?php foreach($exp['tests'] as $idx=>$test): ?>
			<a href="#" class="wpexpro-tab nav-tab" name="wpex-case-<?php echo $idx; ?>"><?php echo ($idx===0?'Experiment Goal':$test['name'])?></a>
		<?php endforeach; ?>
		<a href="#" class="wpexpro-tab nav-tab" id="wpexpro-add-test-case">+ Test Case</a>
		<a href="#" class="button button-primary" id="wpexpro-submit">Save Experiment</a>
	</h2>
	<div class="wpexpro-tab-pane" id="wpexpro-exp-general">
			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row"><label>Experiment Title</label></th>
						<td>
							<input name="title" type="text" value="<?php wpexpro_ehe($exp['title']); ?>" class="regular-text">
							<?php if(isset($errors['title'])):?><p class="description error"><?php wpexpro_ehe($errors['title']); ?></p><?php endif; ?>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><label>Experiment Description</label></th>
						<td>
							<textarea class="regular-text" rows="5" name="description" ><?php wpexpro_ehe($exp['description']); ?></textarea>
							<?php if(isset($errors['description'])):?><p class="description error"><?php wpexpro_ehe($errors['description']); ?></p><?php endif; ?>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><label>Experiment Page</label></th>
						<td> 
							<?php wpexpro_ehe($this->getPostTitle($exp['post_id']));  ?>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><label>Status</label></th>
						<td>
							Experiment is <b><?php echo $this->STATUS_STR[$exp['status']]; ?></b><br/><br/>
							<?php $limit_msg = "You've reached your simultaneous experiment limit. Stop or Pause another experiment to begin this one.<br/>"; ?>
							<?php if($exp['status'] == self::STATUS_NEW): ?>
								<?php if($show_start_button): ?>
									<a href="<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=start" class="button button-primary">Start Experiment</a>
								<?php else: echo $limit_msg; endif;?>
							<?php elseif($exp['status'] == self::STATUS_RUNNING): ?>
								<a href="<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=pause" class="button">Pause Experiment</a>
								<a href="<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=stop" class="button button-red">Stop Experiment</a>
							<?php elseif($exp['status'] == self::STATUS_PAUSED): ?>
								<?php if($show_start_button): ?>
									<a href="<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=start" class="button">Resume Experiment</a>
								<?php else: echo $limit_msg; endif;?>
								<a href="<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=stop" class="button button-red">Stop Experiment</a>
							<?php elseif($exp['status'] == self::STATUS_STOPPED): ?>
								<?php if($show_start_button): ?>
									<a href="<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=start" class="button">Resume Experiment</a>
								<?php else: echo $limit_msg; endif;?>
							<?php endif; ?>
						</td>
					</tr>
				</tbody>
			</table>
				
			<?php if($exp['status'] >= self::STATUS_RUNNING): ?>
				<h2 id='wpexpro-conv-hdr'>Conversion Statistics - 
					<div class='wpexpro-date-picker'><span class='wpexpro-date-picker-label'>All Time</span>
						<div class='wpexpro-date-picker-cont'>
							<ul>
								<li><span>From:</span><input class='wpexpro-date-picker-from' placeholder='July 4th, 8:00pm'/></li>
								<li><span>To:</span><input class='wpexpro-date-picker-to' placeholder='Sept 10th, 10:00pm'/></li>
								<li><span></span><a href="#" class='wpexpro-date-picker-adjust'>Adjust Time</a><a href="#" class='wpexpro-date-picker-all'>Show All</a></li>
							</ul>
						</div>
					</div>
					<div class='wpexpro-chart-refresh'></div>
				</h2>
				<div id="wpexpro-chart">
				</div>
				<div id='wpexpro-chart-data-cont'>
					<h3 name="wpexpro-stat-title">Current Conversion Totals</h3>
					<?php foreach($exp['tests'] as $idx=>$test): ?>
						<div class='wpexpro-chart-data  <?php if($idx === 0):?>first<?php endif; ?>' name="wpexpro-chart-data-<?php echo $test['id']; ?>">
							<div class='data-<?php echo $idx; ?> wpexpro-chart-data-name'><span><?php echo $test['name']; ?></span><!-- <span class='results'><?php echo $test['probability']; ?>%</span> --></div>
							<h2 class='wpexpro-result'><?php echo $test['probability']; ?>%</h2>
							<span class='wpexpro-chart-data-label'>Conversion Rate:</span> 
							<?php if($test['impressions'] == 0): ?>
								<span class='wpexpro-chart-data-percentage wpexpro-chart-data-data' data-total="0%"></span>
							<?php else: ?>
								<span class='wpexpro-chart-data-percentage wpexpro-chart-data-data' data-total="<?php echo round(($test['conversions']/$test['impressions'])*100); ?>%"><?php echo round(($test['conversions']/$test['impressions'])*100); ?>%</span>
							<?php endif; ?>
							<br/>
							<span class='wpexpro-chart-data-label'>Conversions:</span> 
							<span class='wpexpro-chart-data-conversions wpexpro-chart-data-data' data-total="<?php echo $test['conversions']; ?>"><?php echo $test['conversions']; ?></span>
							<br/>
							<span class='wpexpro-chart-data-label'>Impressions:</span> 
							<span class='wpexpro-chart-data-impressions wpexpro-chart-data-data' data-total="<?php echo $test['impressions']; ?>"><?php echo $test['impressions']; ?></span>
							<br/>
						</div>
					<?php endforeach; ?>
				</div>
				<div class='wpexpro-clear-stats'>
					<a href='<?php echo $this->pages['edit'] ?><?php echo $exp['id']; ?>&amp;action=reset' onclick="return confirm('Warning: this cannot be undone. Are you sure you want to reset your experiment result statistics?');">[reset experiment statistics]</a>
					<span class="wpexpro-result-notice"><b>Note:</b> Be careful not to jump to conculsions too early. More impressions yields more accurate results.</span>
				</div>
			<?php endif; ?>
	</div>
	
	<div id="wpexpro-test-tmpl" class="wpexpro-tab-pane" style="display: none;" data-wpexpro-num="">
		<table class="form-table test-case-details">
			<tbody>
				<tr valign="top">
					<th scope="row"><label for="blogname">Test Case Name</label></th>
					<td>
						<input class="wpexpro-test-case-name regular-text" name="experiment[][][name]" type="text" value="" placeholder="Test case name">
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="blogname">Test Case Actions</label></th>
					<td>
						<a class="button button-red wpexpro-test-case-delete">Delete Test</a>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="form-table test-goals">
			<tbody>
				<tr valign="top">
					<th scope="row"><label for="blogname">Experiment Goal</label></th>
					<td>
						<select class="wpexpro-exp-goal-type">
							<option value="<?php echo self::GOAL_CLICK; ?>" <?php if($exp['goal_type'] == self::GOAL_CLICK):?>selected <?php endif; ?>>Click on</option>
							<option value="<?php echo self::GOAL_PAGEVIEW; ?>" <?php if($exp['goal_type'] == self::GOAL_PAGEVIEW):?>selected <?php endif; ?>>View Page</option>
						</select>
						<input class="wpexpro-exp-goal regular-text" type="text" value="" placeholder="Goal Element" <?php if($exp['goal_type'] != self::GOAL_CLICK):?>style="display: none;"<?php endif; ?>>
						<select class="wpexpro-exp-goal-pv" <?php if($exp['goal_type'] != self::GOAL_PAGEVIEW):?>style="display: none;"<?php endif; ?>>
							<option value="">Choose a Page...</option>
							<?php if($inc_root): ?>
								<option value="root" <?php if($exp['goal'] == "root") echo 'selected'; ?>>Home Page</option>
							<?php endif; ?>
							<?php if(count($pages)): ?>
								<optgroup label="Pages">
									<?php foreach($pages as $page): ?>
										<option value="<?php echo $page->ID; ?>" <?php if($exp['goal'] == $page->ID) echo 'selected'; ?>><?php echo $page->post_title; ?></option>
									<?php endforeach; ?>
								</optgroup>
							<?php endif; ?>

							<?php if(count($posts)): ?>
								<optgroup label="Posts">
									<?php foreach($posts as $post): ?>
										<option value="<?php echo $post->ID; ?>" <?php if($exp['goal'] == $post->ID) echo 'selected'; ?>><?php echo $post->post_title; ?></option>
									<?php endforeach; ?>
								</optgroup>
							<?php endif; ?>
						</select>
						<p class="description">Choose your experiment goal by clicking the input box and then selecting an element in the window below or by typing in a custom css selector.</p>
					</td>
				</tr>
			</tbody>
		</table>
		<div class='wpexpro-loading'><img src="<?php echo plugins_url( 'img/loader.gif' , __FILE__ ) ?>"/></div>
		<?php if($exp['post_id'] == 'root'): ?>
			<iframe class="wpexpro-exp" _src="/index.php?noscript=1&amp;inadmin=1" height="100%" width="100%"></iframe>
		<?php else: ?>
			<iframe class="wpexpro-exp" _src="/index.php?p=<?php echo $exp['post_id']; ?>&amp;noscript=1&amp;inadmin=1" height="100%" width="100%"></iframe>
		<?php endif; ?>
	</div>

	<ul id='wpexpro-menu' style='display: none;'>
		<?php if($exp['status'] == self::STATUS_RUNNING): ?>
		<li class='warning'>The experiment is currently running.<br/>Changing a test may skew your results.</li>
		<?php endif; ?>
		<li><a href='#' class='wpexpro-html-editor'>Edit Content</a></li>
		<li class='seperator'></li>
		<li><a href='#' class='wpexpro-class-editor'>Add/Remove Classes</a></li>
		<li><a href='#' class='wpexpro-style-editor'>Adjust Styles</a></li>
		<li class='seperator'></li>
		<li><a href='#' class='wpexpro-delete-elm'>Delete Element</a></li>
		<li class='seperator'></li>
		<li><a href='#' class='wpexpro-add-script'>Edit Custom Javascript</a></li>
	</ul>

	<div id="wpexpro-html-editor" class="reveal-modal xlarge">
		<h1>Edit Element Content</h1>
		<div class="wpexpro-media wp-media-buttons">
			<a href="#" class="button insert-media add_media" data-editor="" title="Add Media"><span class="wp-media-buttons-icon"></span> Add Media</a>
		</div>
		<ul class="wpexpro-editor-tabs">
			<li class="nav-tab wpexpro-editor-visual-tab">Visual</li>
			<li class="nav-tab wpexpro-editor-text-tab">Text</li>
		</ul>
		<p class="wpexpro-editor"><textarea></textarea></p>
		<p class="wpexpro-html" style="display: none;"><textarea></textarea></p>
		<a class="close-reveal-modal">&#215;</a>
		<div class="reveal-modal-buttons">
			<a class="button button-large button-primary wpexpro-save-btn">Save</a>
			<a class="button button-large wpexpro-close-btn">Close</a>
		</div>
	</div>
	
	<div id="wpexpro-script-editor" class="reveal-modal xlarge">
		<h1>Edit Javascript</h1>
		<p class="wpexpro-script"><textarea></textarea></p>
		<p class="wpexpro-result-notice" style="margin: -8px 0 0 15px;">The jQuery javascript framework is available through the <code>jQuery</code> variable.</p>
		<p class="wpexpro-result-notice" style="margin-top: 0;">Your javascript code added as a callback to <code>jQuery(document).ready</code>.</p>
		<a class="close-reveal-modal">&#215;</a>
		<div class="reveal-modal-buttons">
			<a class="button button-large button-primary wpexpro-save-btn">Save</a>
			<a class="button button-large wpexpro-close-btn">Close</a>
		</div>
	</div>

	<div id="wpexpro-class-editor" class="reveal-modal medium">
		<h1>Edit Element Classes</h1>
		<div class='wpexpro-classes-tbl'>
		</div>
		<p>
			<a href="#" class='wpexpro-add-class'>[add new class]</a>
		</p>
		<a class="close-reveal-modal">&#215;</a>
		<div class="reveal-modal-buttons">
			<a class="button button-large button-primary wpexpro-save-btn">Save</a>
			<a class="button button-large wpexpro-close-btn">Close</a>
		</div>
	</div>
	<div id="wpexpro-style-editor" class="reveal-modal medium">
		<h1>Edit Element Styles</h1>
		<div class='wpexpro-styles-tbl'>
		</div>
		<p>
			<a href="#" class='wpexpro-add-style'>[add new style]</a>
		</p>
		<a class="close-reveal-modal">&#215;</a>
		<div class="reveal-modal-buttons">
			<a class="button button-large button-primary wpexpro-save-btn">Save</a>
			<a class="button button-large wpexpro-close-btn">Close</a>
		</div>
	</div>

	<!-- This is here to make sure we get the needed scripts and such -->
	<div style='display: none;'><?php wp_editor( "<p>Test</p>", 'wpexproeditor-worthless' ); ?></div>
</div>